import controller.ServerViewController;
import model.config.NetworkConfig;
import network.ServerManager;
import vista.MainView;

import javax.swing.*;
import java.io.FileNotFoundException;

/**
 * Clase principal del servidor
 */
public class Main {

    /**
     * Metodo principal del servidor
     * @param args argumentos
     */
    public static void main(String[] args) {

        /* Configuracion del servidor y de la base de datos */
        NetworkConfig nConfig;

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            nConfig = NetworkConfig.initialConfig();

            MainView mainView = new MainView();

            ServerViewController controller = new ServerViewController(mainView, nConfig);

            mainView.registerController(controller, controller, controller);

            ServerManager serverManager = ServerManager.getInstance(controller);

            mainView.setVisible(true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}