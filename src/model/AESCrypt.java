package model;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

/**
 * Clase auxiliar para encriptar y decriptar en AES
 *
 * @author Daniel Ortiz
 * @version 1.0.0
 */
public class AESCrypt {

    private static final byte[] PASSPHRASE = "quieroAprobarDPO".getBytes(Charset.forName("UTF-8"));
    private static final String ALGORITHM = "AES";
    private static final Key KEY = new SecretKeySpec(PASSPHRASE, ALGORITHM);

    /**
     * Encripta
     *
     * @param data datos a encriptar
     * @return string cifrada en base64 de los datos encriptados
     */
    public static String encrypt(char[] data) {
        byte[] bytes = toBytes(data);
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, KEY);
            return Base64.getEncoder().encodeToString(cipher.doFinal(bytes));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Decripta
     *
     * @param data datos cifrados en base64
     * @return string de los datos desencriptados
     */
    public static String decrypt(String data) {
        Cipher cipher;
        try {
            cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, KEY);
            return new String(cipher.doFinal(Base64.getDecoder().decode(data.getBytes(Charset.forName("UTF-8")))));
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Convierte de char[] a byte[], formato UTF-8
     *
     * @param data datos a convertir
     * @return byte[] convertido
     */
    private static byte[] toBytes(char[] data) {
        CharBuffer charBuffer = CharBuffer.wrap(data);
        ByteBuffer byteBuffer = Charset.forName("UTF-8").encode(charBuffer);
        byte[] bytes = Arrays.copyOfRange(byteBuffer.array(),
                byteBuffer.position(), byteBuffer.limit());
        Arrays.fill(charBuffer.array(), '\u0000');
        Arrays.fill(byteBuffer.array(), (byte) 0);
        return bytes;
    }
}