package model;

import identicon.IdenticonGenerator;
import network.packet.*;
import network.packet.request.Register;
import org.apache.commons.codec.digest.DigestUtils;
import util.CurrentDate;
import util.FileUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Clase de comunicacion con la base de datos
 */
public class Database {

    /**
     * Constantes para los nombres de cada tabla y de sus columnas
     * el formato que siguen es NOMBRETABLA_TABLE o NOMBRETABLA_NOMBRECOLUMNA
     * NOMBRETABLA siempre ira to.do junto, NOMBRECOLUMNA puede contener '_'
     * los nombres de las constantes estan en ingles pero en la base de datos
     * esta en castellano to.do...
     */

    /* TABLA Usuario */
    private static final String USER_TABLE = "Usuario";
    private static final String USER_EMAIL = "correo";
    private static final String USER_NAME = "nombre";
    private static final String USER_PASSWORD = "contrasena";
    private static final String USER_IMAGE = "imagen";
    private static final String USER_REGISTRATION_DATE = "registro";
    private static final String USER_LAST_CONNEXION = "ultima_conexion";
    private static final String USER_IS_DELETED = "eliminado";

    /* TABLA Canal */
    private static final String CHANNEL_TABLE = "Canal";
    private static final String CHANNEL_ID = "id_canal";
    private static final String CHANNEL_NAME = "nombre";
    private static final String CHANNEL_DESCRIPTION = "descripcion";
    private static final String CHANNEL_IS_PRIVATE = "esPrivado";

    /* TABLA UsuarioCanal */
    private static final String USERCHANNEL_TABLE = "UsuarioCanal";
    private static final String USERCHANNEL_ID = "id_canal";
    private static final String USERCHANNEL_EMAIL = "correo";
    private static final String USERCHANNEL_LAST_VIEWED_MESSAGE = "ultimo_mensaje_leido";

    /* TABLA Mensaje */
    private static final String MESSAGE_TABLE = "Mensaje";
    private static final String MESSAGE_ID = "id_mensaje";
    private static final String MESSAGE_EMAIL = "correo";
    private static final String MESSAGE_DATE = "fecha";
    private static final String MESSAGE_CHANNEL_ID = "id_canal";

    /* TABLA MensajeTexto */
    private static final String TEXTMESSAGE_TABLE = "MensajeTexto";
    private static final String TEXTMESSAGE_ID = "id_mensaje_texto";
    private static final String TEXTMESSAGE_TEXT = "texto";

    /* TABLA Imagen */
    private static final String IMAGEMESSAGE_TABLE = "Imagen";
    private static final String IMAGEMESSAGE_ID = "id_imagen";
    private static final String IMAGEMESSAGE_IMAGE = "ruta";

    /* TABLA Archivo */
    private static final String FILEMESSAGE_TABLE = "Archivo";
    private static final String FILEMESSAGE_ID = "id_archivo";
    private static final String FILEMESSAGE_DATA = "datos";
    private static final String FILEMESSAGE_FILENAME = "nombre";

    private Connection conn;
    private SimpleDateFormat formatter;

    /**
     * Constuctor de la clase
     */
    public Database() {
        //String url = "jdbc:mysql://138.68.172.67:3306/ac15?autoReconnect=true&useSSL=false";
        String url = "jdbc:mysql://138.68.172.67:3306/slaibox?autoReconnect=true&useSSL=false";
        String user = "daniel";
        String password = "aprobar dpo2";
        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            //Aconseguim connexió a la base de dades que tenim en localhost

            DriverManager.setLoginTimeout(5);
            conn = DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Genera db a partir de parametros
     * @param user usuario
     * @param password contraseña
     * @param name nombre
     * @param ip ip
     * @param port puerto
     */
    public Database(String user, String password, String name, String ip, int port) {
        //String url = "jdbc:mysql://138.68.172.67:3306/ac15?autoReconnect=true&useSSL=false";
        String url = "jdbc:mysql://" + ip + ":" + port +"/" + name + "?autoReconnect=true&useSSL=false";
        //String user = "daniel";
        //String password = "aprobar dpo2";
        formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            //Aconseguim connexió a la base de dades que tenim en localhost

            DriverManager.setLoginTimeout(5);
            conn = DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Cierra la db
     */
    public synchronized void close() {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Actualiza el estado baneado del usuario
     * @param email correo del usuario
     * @param status booleano baneado true. no baneado false.
     */
    public void setBannedStatus(String email, Boolean status) {
        try(Statement stmt = conn.createStatement()) {
            String sql =    "UPDATE " + USER_TABLE + " SET " + USER_IS_DELETED
                            + " = " + status.toString() + " WHERE " + USER_EMAIL
                            + " = '" + email + "'";
            stmt.executeUpdate(sql);

        } catch (SQLException e) {
            System.out.println("No se encuentra al usuario");
        }
    }


    /**
     * a siguiente función inserta un usuario en la base de datos.
     * A partir de un correo, nombre, contraseña y una imagen se genera un sql con los datos y se ejecuta.
     * @param correo correo
     * @param nombre nombre
     * @param password password
     * @param imagen imagen
     * @return id si se pudo insertar bien o no
     */
    public int insertUser(String correo, String nombre, String password, BufferedImage imagen) {
        String date = new CurrentDate().toString();
        String sql = "INSERT INTO Usuario(correo, nombre, contrasena, imagen, registro, ultima_conexion, eliminado) " +
                //"VALUES ('" + correo + "', '" + nombre + "', '" + password + "', '" + imagen +  "', '" +
                "VALUES ('" + correo + "', '" + nombre + "', '" + password + "', ?, '" +
                date + "', '" + date + "', FALSE);";
        try (PreparedStatement stmt = conn.prepareStatement(sql)){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ImageIO.write(imagen, "png", out);
            stmt.setBlob(1, new ByteArrayInputStream(out.toByteArray()));
            stmt.execute();
            return 1;
        } catch (SQLException | IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Actualiza la última conexión de un usuario
     * @param email correo del usuario
     * @param date nueva última fecha de conexión
     */
    public void updateLastConnectionTime(String email, Date date){
        try (Statement stmt = conn.createStatement()) {
            String sql = "UPDATE " + USER_TABLE + " SET " + USER_LAST_CONNEXION + " = '" + formatter.format(date) + "' WHERE " + USER_EMAIL + " = '" + email + "'";
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Inserta un usuario a un canal
     * @param correo email del usuario
     * @param idCanal id del canal
     * @return devuelve int para saber si se ha insertado. > 0 quiere decir que se afectaron filas y se insertó.
     * Si es -1 hubo una excepción
     */
    public int insertUserChannel(String correo, int idCanal) {
        try (Statement stmt = conn.createStatement()) {
            String sql = "INSERT INTO UsuarioCanal(correo, id_canal) " +
                            "VALUES ('" + correo + "', " + idCanal + ")";
            return stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Ésta función inserta un mensaje en un canal sabiendo el correo de la persona que ha generado
     * éste mensaje y la fecha en que lo ha enviado.
     * Hay que tener en cuenta que después de insertar un Mensaje, hay que insertar también en la tabla
     * "hija", las que heredan de Mensaje. (MensajeTexto, Archivo, Imagen)
     *
     * @param email -> correo de la persona que genera el mensaje en formato String
     * @param channelId -> el ID del canal donde se ha enviado el mensaje en formato int
     * @param date -> la fecha de envío del mensaje en formato Date
     * @param message -> el mensaje en formato String
     */
    public int insertMessage(String email, int channelId, Date date, String message) {
        String sql = "INSERT INTO Mensaje(correo, id_canal, fecha) " +
                "VALUES ('" +  email + "', " + channelId + ", '" + formatter.format(date) + "')";
        try (PreparedStatement stmt = conn.prepareStatement(sql, new String[] {"id_mensaje"})) {
            if (stmt.executeUpdate() > 0) {
                try(ResultSet rs = stmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        int messageId = rs.getInt(1);
                        String sql2 = "INSERT INTO MensajeTexto(id_mensaje_texto, texto)" +
                                "VALUES(" + messageId + ", ?)";
                        try (PreparedStatement stmt2 = conn.prepareStatement(sql2)) {
                            stmt2.setString(1, message);
                            updateLastMessageRead(email, channelId, messageId);
                            stmt2.executeUpdate();
                            return messageId;
                        }
                    }
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Ésta función inserta un mensaje en un canal sabiendo el correo de la persona que ha generado
     * éste mensaje y la fecha en que lo ha enviado.
     * Hay que tener en cuenta que después de insertar un Mensaje, hay que insertar también en la tabla
     * "hija", las que heredan de Mensaje. (MensajeTexto, Archivo, Imagen)
     *
     * @param email -> correo de la persona que genera el mensaje en formato String
     * @param channelId -> el ID del canal donde se ha enviado el mensaje en formato int
     * @param date -> la fecha de envío del mensaje en formato Date
     * @param data -> archivo File
     */
    public int insertFile(String email, int channelId, Date date, byte[] data, String nombre) {
        String sql = "INSERT INTO Mensaje(correo, id_canal, fecha) " +
                "VALUES ('" +  email + "', " + channelId + ", '" + formatter.format(date) + "')";
        try (PreparedStatement stmt = conn.prepareStatement(sql, new String[] {"id_mensaje"})) {
            if (stmt.executeUpdate() > 0) {
                try(ResultSet rs = stmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        int messageId = rs.getInt(1);
                        String sql2 = "INSERT INTO Archivo(id_archivo, datos, nombre) VALUES( " + messageId + ", ? ,'" + nombre + "')";
                        try (PreparedStatement stmt2 = conn.prepareStatement(sql2)) {
                            stmt2.setBytes(1, data);
                            updateLastMessageRead(email, channelId, messageId);
                            stmt2.executeUpdate();
                            return messageId;
                        }
                    }
                }
            }
        } catch (SQLException  e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Dice si existe el nombre de usuario en la base de datos.
     *
     * @param user, el nombre de usuario a consultar
     * @return true si el usuario existe o se ha producido un error, falso si el usuario no existe
     */
    public boolean existsUser(String user) {
        try (Statement stmt = conn.createStatement()) {
            String sql = "SELECT COUNT(nombre) AS q FROM Usuario WHERE nombre LIKE '" + user + "';";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt(1) == 1;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Dice si existe el email en la base de datos.
     *
     * @param email email a consultar
     * @return true si existe o se ha producido un error, falso si el usuario no existe
     */
    public boolean existsEmail(String email) {
        try (Statement stmt = conn.createStatement()) {
            String sql = "SELECT count(correo) AS q FROM Usuario WHERE correo LIKE '" + email + "';";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getInt(1) == 1;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Comprueba si la contraseña coincide con el usuario
     * @param user String correo del usuario
     * @param pw String contraseña
     * @return retorna booleano indicando si coincide (true) o no coincide (false)
     */
    public boolean pwCoincide(String user, String pw) {
        try (Statement stmt = conn.createStatement()) {
            String aux = hashPassword(AESCrypt.decrypt(pw).toCharArray());
            String sql = "SELECT COUNT(correo) AS pw FROM  Usuario WHERE correo = '" + user + "' AND contrasena = '"
                    + aux + "' OR nombre = '" + user + "' AND contrasena = '" + aux + "';";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                if (rs.next()){
                    return rs.getInt(1) == 1;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Intenta registrar un usuario en la base de datos.
     *
     * @param request peticion de registro
     * @return 0 error al inserir el usuario en la base de datos
     * 1 si ha funcionado correctamente
     * -1 si ya existe el usuario o el correo
     * -2 si no se puede escribir el identicon localmente
     * -3 no se puede crear la carpeta donde se almacenan los identicon
     */
    public int registerUser(Register request) {
        if (!existsUser(request.getEmailUser()) && !existsEmail(request.getEmail())) {
            String hash = hashPassword(AESCrypt.decrypt(request.getPassword()).toCharArray());
            BufferedImage icon = IdenticonGenerator.generate(request.getEmailUser());
            return insertUser(request.getEmail(), request.getEmailUser(), hash, icon);
        }
        return -1;
    }

    /**
     * Método privado para encriptar una contraseña. Sirve para insertar un usuario con su contraseña encriptada (Registro)
     * y también para comprovar si el la contraseña coincide con el usuario (Login)
     * @param pwd contraseña del usuario sin encriptar
     * @return contraseña del usuario encriptada
     */
    private String hashPassword(char[] pwd) {
        return DigestUtils.sha512Hex(new String(pwd));
    }

    /**
     * Función para saber qué tipo de mensaje es. La tabla Mensaje en la DB es "madre" de 3 tipos de mensaje.
     *
     * @param messageId -> id del mensaje
     * @return -> retorna tipo del mensaje en formato String: ("texto", "archivo" o "imagen")
     */
    private String getTypeOfMessage(int messageId) {
        String typeOfMessage = null;

        try (Statement stmt = conn.createStatement()) {
            String sql1 = "SELECT COUNT(*) FROM MensajeTexto WHERE id_mensaje_texto = " + messageId + ";";
            try (ResultSet rs1 = stmt.executeQuery(sql1)) {
                if (rs1.next()){
                    if (rs1.getInt(1) == 0) {
                        typeOfMessage = "archivo";
                    } else {
                        typeOfMessage = "texto";
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return typeOfMessage;
    }

    /**
     * Función para obtener los usuarios que estan en un canal concreto
     *
     * @param id_channel -> ID del canal en formato int
     * @return -> retorna un ArrayList de tipo String que contiene los correos de los usuarios que estan en el canal indicado por parámetro
     */
    public ArrayList<String> getUsersFromChannel(int id_channel) {
        ArrayList<String> listUsers = new ArrayList<>();

        try (Statement stmt = conn.createStatement()) {
            String sql = "SELECT correo FROM UsuarioCanal WHERE id_canal = " + id_channel + " ORDER BY correo ASC";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    String user = rs.getString(USERCHANNEL_EMAIL);
                    listUsers.add(user);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listUsers;
    }

    /**
     * Función para obtener los canales en los que está un usuario
     * Para ello también hay que llamar a dos funciones de esta misma clase para obtener todos los mensajes
     * y todos los usuarios que hay en cada canal.
     *
     * @param email -> correo del usuario en formato String
     * @return -> retorna un ArrayList de tipo Channel que seran todos en los que está el usuario
     */
    public HashMap<Integer, Channel> getChannelsFromUser(String email) {
        HashMap<Integer, Channel> listChannels = new HashMap<>();

        try (Statement stmt = conn.createStatement()) {
            String sql =    "SELECT * FROM " + CHANNEL_TABLE + " AS c JOIN " + USERCHANNEL_TABLE
                            + " AS uc ON(c." + CHANNEL_ID + " = uc." + USERCHANNEL_ID + ") WHERE "
                            + USERCHANNEL_EMAIL + " = '" + email +"'";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    int channelId = rs.getInt(CHANNEL_ID);
                    ArrayList<Message> listMessages = null;

                    ArrayList<String> listUsers = getUsersFromChannel(channelId);
                    ArrayList<File> filesInfo = getFilesFromChannel(channelId);
                    int totalMessages = getCountMessagesFromChannel(channelId);
                    Channel channel = new Channel(rs.getString(CHANNEL_NAME), rs.getString(CHANNEL_DESCRIPTION), listUsers,
                            rs.getBoolean(CHANNEL_IS_PRIVATE), channelId, listMessages, rs.getInt(USERCHANNEL_LAST_VIEWED_MESSAGE),
                            filesInfo,totalMessages);
                    channel.setUnreadMessagesNumber(countMessageUnread(email, channelId));
                    listChannels.put(channel.getId(), channel);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listChannels;
    }

    /**
     * Función para obtener los canales en los que está un usuario
     * Para ello también hay que llamar a dos funciones de esta misma clase para obtener todos los mensajes
     * y todos los usuarios que hay en cada canal.
     *
     * @param correo -> correo del usuario en formato String
     * @return -> retorna un ArrayList de tipo Channel que seran todos en los que está el usuario
     */
    public ArrayList<Integer> getChannelsIdFromUser(String correo) {
        ArrayList<Integer> listChannels = new ArrayList<>();

        try (Statement stmt = conn.createStatement()) {
            String sql = "SELECT id_canal FROM Canal WHERE id_canal IN(SELECT id_canal FROM UsuarioCanal WHERE correo = '" + correo + "')";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    listChannels.add(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listChannels;
    }

    /**
     * Función que obtiene todos los usuarios registrados en la base de datos.
     *
     * @return -> retorna un ArrayList de tipo Profile
     */
    public Map<String, Profile> getRegisteredUsers() {
        Map<String, Profile> registeredUsers = Collections.synchronizedMap(new HashMap<>());

        try (Statement stmt = conn.createStatement()) {
            String sql = "SELECT * FROM Usuario;";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    Profile profile = getProfileFromResultSet(rs);
                    registeredUsers.put(profile.getEmail(), profile);
                }
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        return registeredUsers;
    }

    /**
     * Función para obtener todos los canales y ponerlos en una tabla de hash
     * @return retorna la tabla de hash con la clave el id del canal y el valor el canal
     */
    public Map<Integer, Channel> getAllChannelsInfo() {
        Map<Integer, Channel> channels = Collections.synchronizedMap(new HashMap<>());
        try (Statement stmt = conn.createStatement()) {
            String sql = "SELECT * FROM Canal;";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    int channelId = rs.getInt("id_canal");

                    ArrayList<String> listUsers = getUsersFromChannel(channelId);
                    ArrayList<File> filesInfo = getFilesFromChannel(channelId);
                    int totalMessages = getCountMessagesFromChannel(channelId);
                           Channel channel = new Channel(rs.getString(CHANNEL_NAME), rs.getString(CHANNEL_DESCRIPTION), listUsers,
                                    rs.getBoolean(CHANNEL_IS_PRIVATE), channelId, null, 0,
                                    filesInfo,totalMessages);
                    channels.put(channel.getId(), channel);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return channels;
    }



    /**
     * Función para obtener todos los mensajes de un canal
     *
     * @param id_channel -> ID del canal en formato INT del cual se quieren obtener todos sus mensajes
     * @return -> retorna ArrayList de tipo Mensaje
     */
    public ArrayList<Message> getMessagesFromChannel(int id_channel, int offset) {
        ArrayList<Message> listMessage = new ArrayList<>();
        String typeOfMessage;
        Message message = null;
        int messageId;

        offset = offset*100;

        try (Statement stmt = conn.createStatement()) {
            String sql = "SELECT id_mensaje FROM Mensaje " +
                    "WHERE id_canal = " + id_channel + " ORDER BY fecha DESC LIMIT 100 OFFSET " + offset;
            try (ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    messageId = rs.getInt(MESSAGE_ID);
                    typeOfMessage = getTypeOfMessage(messageId);
                    switch(typeOfMessage) {
                        case "texto":
                            sql = "SELECT * FROM Mensaje JOIN MensajeTexto ON" +
                                    " (id_mensaje = id_mensaje_texto)" +
                                    "WHERE id_mensaje_texto = " + messageId + ";";
                            break;
                        case "archivo":
                            sql = "SELECT * FROM Mensaje JOIN Archivo ON" +
                                    " (id_mensaje = id_archivo)" +
                            "WHERE id_archivo = " + messageId + ";";
                            break;
                    }
                    try (Statement stmt2 = conn.createStatement()) {
                        try (ResultSet rs2 = stmt2.executeQuery(sql)) {
                            if (rs2.next()) {
                                switch (typeOfMessage) {
                                    case "texto":

                                        message = new Text(rs2.getString(MESSAGE_EMAIL),
                                                rs2.getInt(MESSAGE_CHANNEL_ID),
                                                rs2.getString(TEXTMESSAGE_TEXT),
                                                rs2.getInt(MESSAGE_ID));
                                        break;
                                    case "archivo":
                                        try {
                                            byte[] data = rs2.getBytes(FILEMESSAGE_DATA);
                                            message = new File(rs2.getString(MESSAGE_EMAIL), data,
                                                    rs2.getString(FILEMESSAGE_FILENAME),rs2.getInt(MESSAGE_CHANNEL_ID),
                                                    rs2.getInt(MESSAGE_ID));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        break;
                                }
                                if (message != null) {
                                    message.setDate(rs2.getTimestamp(MESSAGE_DATE));
                                    listMessage.add(message);
                                }
                            }
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listMessage;
    }

    /**
     * Inserta un canal
     * @param channelName nombre del canal
     * @param channelDescription descripción del canal
     * @param isPrivate atributo boolean de si es privado o no
     * @return int para saber si se ha insertado correctamente el canal. si es -1 es que no se ha insertado.
     */
    public int insertChannel(String channelName, String channelDescription, boolean isPrivate) {
        String sql = "INSERT INTO Canal(nombre, descripcion, esPrivado) VALUES('" + channelName + "','" + channelDescription + "'," + isPrivate + ");";
        String[] values = new String[]{CHANNEL_ID};
        try (PreparedStatement stmt = conn.prepareStatement(sql, values)) {
            if (stmt.executeUpdate() > 0) {
                try (ResultSet rs = stmt.getGeneratedKeys()) {
                    if (rs.next()){
                        return rs.getInt(1);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * Funcion que obtiene todos los datos de un usuario a partir de su correo
     *
     * @param email -> correo del usuario en tipo String del cual se quieren obtener sus demás datos
     * @return -> retorna tipo Profile que contiene todos los datos del usuario
     */
    public Profile getUserFromEmail(String email) {
        return getUser(email, USER_EMAIL);
    }

    public Profile getUserFromUsername(String username) {
        return getUser(username, USER_NAME);
    }

    /**
     * Obtiene el perfil de un usuario a partir de su id
     * @param userId id del usuario
     * @param column
     * @return devuelve objeto Profile con el perfil del usuario que contiene todos sus datos.
     */
    public Profile getUser(String userId, String column) {
        try (Statement stmt = conn.createStatement()) {
            String sql = "SELECT * FROM Usuario WHERE " + column + " = '" + userId + "'";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                if (rs.next()) {
                    return getProfileFromResultSet(rs);
                }
            }
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Obtiene el correo de un usuario a partir de su nombre
     * @param username nombre del usuario
     * @return devuelve String con el correo del usuario
     */
    public String getEmailFromUsername(String username) {
        try (Statement stmt = conn.createStatement()) {
            String sql = "SELECT " + USER_EMAIL + " FROM " + USER_TABLE + " WHERE " + USER_NAME + " = '" + username + "'";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                if (rs.next()) {
                    return rs.getString(USER_EMAIL);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Método privado que obtiene un perfil a través de un ResultSet generado por una query.
     * Aquí se crea el perfil comprovando si está eliminado o desconectado el usuario y obteniendo su imagen en formato blob.
     * @param rs ResultSet
     * @return devuelve objeto de la clase Profile
     * @throws SQLException
     * @throws IOException
     */
    private Profile getProfileFromResultSet(ResultSet rs) throws SQLException, IOException {
        String status;
        if (rs.getBoolean(USER_IS_DELETED)) {
            status = Profile.DELETED;
        } else {
            status = Profile.DISCONNECTED;
        }
        Blob blob = rs.getBlob(USER_IMAGE);

        BufferedImage image = ImageIO.read(blob.getBinaryStream());

        return new Profile(rs.getString(USER_NAME), rs.getString(USER_EMAIL),
                image, status, getChannelsIdFromUser(rs.getString(USER_EMAIL)),
                rs.getTimestamp(USER_LAST_CONNEXION), rs.getTimestamp(USER_REGISTRATION_DATE));
    }

    /**
     * Función para saber todos los usuarios que estan en una lista de canales.
     * Obtiene una tabla de hash con la clave el correo del usuario y valor el perfil (objeto de Profile) del usuario.
     * A partir de otra tabla de hash con la clave id del canal y valor el canal (objeto de Channel).
     * @param channels tabla de hash con clave id del canal y valor el canal
     * @return devuelve tabla de hash con clave correo del usuario y valor perfil del usuario
     */
    public HashMap<String, Profile> getUsersOnClientChannels(HashMap<Integer, Channel> channels) {
        HashMap<String, Profile> profiles = new HashMap<>();
        if (channels != null && !channels.isEmpty()) {
            try (Statement stmt = conn.createStatement()) {
                String sql = "SELECT DISTINCT * FROM " + USER_TABLE
                        + " AS u JOIN " + USERCHANNEL_TABLE + " AS uc ON (u."
                        + USER_EMAIL + " = uc." + USERCHANNEL_EMAIL + ")"
                        + " WHERE " + USERCHANNEL_ID + " IN (";
                StringBuilder sb = new StringBuilder(sql);
                List<Integer> channelsId = new ArrayList<>(channels.keySet());
                int size = channelsId.size();
                for (int i = 0; i < size; ++i) {
                    sb.append(channelsId.get(i));
                    if (i < size - 1) {
                        sb.append(", ");
                    } else {
                        sb.append(")");
                    }
                }
                try (ResultSet rs = stmt.executeQuery(sb.toString())) {
                    while (rs.next()) {
                        Profile p = getProfileFromResultSet(rs);
                        profiles.put(p.getEmail(), p);
                    }
                }

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
        return profiles;
    }

    /**
     * Función para obtener la cantidad total de mensajes en un canal recibiendo el id de éste
     * @param channelId id del canal
     * @return int con la cantidad de mensajes
     */
    public int getCountMessagesFromChannel(int channelId) {
        int count = 0;

        try (Statement stmt = conn.createStatement()) {
            String sql = "select count(*) from Mensaje WHERE " + MESSAGE_CHANNEL_ID + " = " + channelId + ";";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                if (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Esta función sirve para conocer la cantidad de mensajes de texto en la última semana, por cada dia para la gráfica de estadísticas.
     * @param channelId id del canal
     * @return array de 7 casillas int con la cantidad de mensajes de texto (7 casillas = 7 dias de la semana)
     */
    public int[] getCountTextsLastWeek(int channelId) {
        int[] counts = new int[7];

        try (Statement stmt = conn.createStatement()) {
            for (int i = 0; i < 7; i++) {
                String sql = "select count(*) AS count from Mensaje, MensajeTexto where DATE(" + MESSAGE_DATE + ") " +
                        "= DATE_SUB(CURDATE(), INTERVAL 6-" + i + " DAY) AND " + TEXTMESSAGE_ID + " = " + MESSAGE_ID +
                        " AND " + MESSAGE_CHANNEL_ID + " = " + channelId + ";";
                try (ResultSet rs = stmt.executeQuery(sql)) {
                    if (rs.next()) {
                        counts[i] = rs.getInt("count");
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return counts;
    }

    /**
     * Esta función sirve para conocer la cantidad de archivos en la última semana, por cada dia para la gráfica de estadísticas.
     * @param channelId id del canal
     * @return array de 7 casillas int con la cantidad de archivos (7 casillas = 7 dias de la semana)
     */
    public int[] getCountFilesLastWeek(int channelId) {
        int[] counts = new int[7];

        try (Statement stmt = conn.createStatement()) {
            for (int i = 0; i < 7; i++) {
                String sql = "select count(*) AS count from Mensaje, Archivo where DATE(" + MESSAGE_DATE + ") " +
                        "= DATE_SUB(CURDATE(), INTERVAL 6-" + i + " DAY) AND " + FILEMESSAGE_ID + " = " + MESSAGE_ID +
                        " AND " + MESSAGE_CHANNEL_ID + " = " + channelId + "";
                try (ResultSet rs = stmt.executeQuery(sql)) {
                    if (rs.next()) {
                        counts[i] = rs.getInt("count");
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return counts;
    }

    /**
     * Esta función sirve para conocer la cantidad de imágenes en la última semana, por cada dia para la gráfica de estadísticas.
     * @param channelId id del canal
     * @return array de 7 casillas int con la cantidad de imágenes (7 casillas = 7 dias de la semana)
     */
    public int[] getCountPicturesLastWeek(int channelId) {
        int[] counts = new int[7];
        for (int i = 0; i < counts.length; i++) {
            counts[i] = 0;
        }

        try (Statement stmt = conn.createStatement()) {
            for (int i = 0; i < 7; i++) {
                String sql = "SELECT " + FILEMESSAGE_FILENAME + " FROM " + MESSAGE_TABLE + ", " + FILEMESSAGE_TABLE
                        + " WHERE DATE(" + MESSAGE_DATE + ") = DATE_SUB(CURDATE(), INTERVAL 6-" + i + " DAY) AND "
                        + FILEMESSAGE_ID + " = " + MESSAGE_ID + " AND " + MESSAGE_CHANNEL_ID + " = " + channelId + "";
                try (ResultSet rs = stmt.executeQuery(sql)) {
                    while (rs.next()) {
                        if (FileUtils.isPicture(rs.getString(FILEMESSAGE_FILENAME))) {
                            counts[i]++;
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return counts;
    }

    /**
     * Obtener el último mensaje leido de un usuario en un canal
     * @param email correo del usuario
     * @param channelId id del canal
     * @return int con el id del mensaje
     */
    public int getLastMessageRead(String email, int channelId) {
        int ultimo_mensaje_leido = 0;

        try (Statement stmt = conn.createStatement()) {
            String sql = "SELECT " + USERCHANNEL_LAST_VIEWED_MESSAGE + " FROM " + USERCHANNEL_TABLE + " WHERE "
                    + USERCHANNEL_EMAIL + " = '" + email + "' AND " + USERCHANNEL_ID + " = " + channelId;
            try (ResultSet rs = stmt.executeQuery(sql)) {
                if (rs.next()) {
                    ultimo_mensaje_leido = rs.getInt(1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return ultimo_mensaje_leido;
    }

    /**
     * Actualiza el último mensaje leido de un usuario en un canal. Así luego sabemos cuántos mensajes no ha leído en el canal.
     * @param email correo del usuario
     * @param channelId id del canal
     * @param messageId id del mensaje que leyó por último el usuario
     */
    public void updateLastMessageRead(String email, int channelId, int messageId) {
        try (Statement stmt = conn.createStatement()){
            if (messageId != 0) {
                String sql = "UPDATE " + USERCHANNEL_TABLE + " SET " + USERCHANNEL_LAST_VIEWED_MESSAGE + " = "
                        + messageId + " WHERE " + USERCHANNEL_EMAIL + " = '" + email + "' AND " + USERCHANNEL_ID + " = " + channelId;
                stmt.executeUpdate(sql);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Obtiene una lista con los archivos totales de un canal (Archivos y imágenes).
     * Si es un archivo se guarda en la lista sin el data. Si es una imágen se guarda con el data para poner la miniatura en el cliente.
     * @param channelId id del canal
     * @return Devuelve ArrayList del objeto de la clase File.
     */
    public ArrayList<File> getFilesFromChannel(int channelId) {
        ArrayList<File> files = new ArrayList<>();

        try (Statement stmt = conn.createStatement()) {
            String sql = "SELECT " + MESSAGE_ID + ", " + FILEMESSAGE_FILENAME + ", " + MESSAGE_DATE + ", " + MESSAGE_EMAIL + ", "
                    + FILEMESSAGE_DATA + " FROM " + MESSAGE_TABLE + ", " + FILEMESSAGE_TABLE
                    + " WHERE " + MESSAGE_ID + " = " + FILEMESSAGE_ID + " AND " + CHANNEL_ID + " = " + channelId;
            try (ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    if (FileUtils.isPicture(rs.getString(FILEMESSAGE_FILENAME))) {
                        File file = new File(rs.getInt(MESSAGE_ID), rs.getString(FILEMESSAGE_FILENAME), rs.getBytes(FILEMESSAGE_DATA));
                        files.add(file);
                    } else {
                        File file = new File(rs.getInt(MESSAGE_ID), rs.getString(FILEMESSAGE_FILENAME));
                        file.setDate(rs.getTimestamp(MESSAGE_DATE));
                        file.setEmailUser(rs.getString(MESSAGE_EMAIL));
                        files.add(file);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return files;
    }

    /**
     * Obtener el archivo completo a partir de su id
     * @param fileId id del archivo
     * @return devuelve objeto de la clase File con toda la información del archivo
     */
    public File getCompleteFile(int fileId) {
        File file = null;
        try(Statement stmt = conn.createStatement()) {
            String sql = "SELECT * FROM " + MESSAGE_TABLE + ", " + FILEMESSAGE_TABLE +
            " WHERE " + MESSAGE_ID + " = " + FILEMESSAGE_ID + " AND " + FILEMESSAGE_ID + " = " + fileId;
            try (ResultSet rs = stmt.executeQuery(sql)) {
                if (rs.next()) {
                    byte[] data = rs.getBytes(FILEMESSAGE_DATA);
                    file = new File(rs.getString(MESSAGE_EMAIL), data,
                            rs.getString(FILEMESSAGE_FILENAME),rs.getInt(MESSAGE_CHANNEL_ID),
                            rs.getInt(MESSAGE_ID));

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("No existe la id del file");
        }
        return file;
    }

    /**
     * Elimina un canal a través de su id
     * @param channelId id del canal
     * @return devuelve > 0 si se eliminó el canal. Si no, devuelve 0.
     */
    public int deleteChannel(int channelId) {
        int affectedRows = 0;
        try (Statement stmt = conn.createStatement()) {
            String sql = "DELETE FROM " + TEXTMESSAGE_TABLE + " WHERE " + TEXTMESSAGE_ID + " IN (SELECT "
                    + MESSAGE_ID + " FROM " + MESSAGE_TABLE + " WHERE " + CHANNEL_ID + " = " + channelId + ")";
            stmt.executeUpdate(sql);

            sql = "DELETE FROM " + FILEMESSAGE_TABLE + " WHERE " + FILEMESSAGE_ID + " IN (SELECT "
                    + MESSAGE_ID + " FROM " + MESSAGE_TABLE + " WHERE " + CHANNEL_ID + " = " + channelId + ")";
            stmt.executeUpdate(sql);

            sql = "DELETE FROM " + USERCHANNEL_TABLE + " WHERE " + CHANNEL_ID + " = " + channelId;
            stmt.executeUpdate(sql);

            sql = "DELETE FROM " + MESSAGE_TABLE + " WHERE " + CHANNEL_ID + " = " + channelId;
            stmt.executeUpdate(sql);

            sql = "DELETE FROM " + CHANNEL_TABLE + " WHERE " + CHANNEL_ID + " = " + channelId;
            affectedRows = stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return affectedRows;
    }

    /**
     * Obtiene la cantidad de mensajes no leídos de un usuario en un canal
     * @param userEmail correo del usuario
     * @param channelId id del canal
     * @return devuelve int que es la cantidad de mensajes no leídos
     */
    public int countMessageUnread(String userEmail, int channelId) {
        int count = 0;

        try (Statement stmt = conn.createStatement()) {
            int lastMessageRead = getLastMessageRead(userEmail, channelId);
            String sql = "SELECT COUNT(*) AS count FROM " + MESSAGE_TABLE + " WHERE " + MESSAGE_CHANNEL_ID + " = " + channelId
                    + " AND " + MESSAGE_ID + " > " + lastMessageRead;
            try (ResultSet rs = stmt.executeQuery(sql)) {
                if (rs.next()) {
                    count = rs.getInt("count");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }
}