package model.config;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Clase para configurar la comm con base de datos y del server scoket
 */
public class NetworkConfig {
    /** Parametros de configuracion */
    private int port_server;
    private String db_name;
    private String db_user;
    private String db_pass;
    private String db_ip;
    private int db_port;

    /**
     * devuelve el puerto usado para conectarse con el cliente
     * @return
     */
    public int getPort_server() {
        return port_server;
    }

    /**
     * devuelve el nombre de la base de datos a la cual hay que conectarse
     * @return
     */
    public String getDb_name() {
        return db_name;
    }

    /**
     * devuelve el usuario de la base de datos a la cual hay que conectarse
     * @return
     */
    public String getDb_user() {
        return db_user;
    }

    /**
     * devuelve la contraseña del usuariro de la base de datos a la cual hay que conectarse
     * @return
     */
    public String getDb_pass() {
        return db_pass;
    }

    /**
     * devuelve la ip de la base de datos a la cual hay que conectarse
     * @return
     */
    public String getDb_ip() {
        return db_ip;
    }

    /**
     * puerto usado para conectarse con la base de datos
     * @return
     */
    public int getDb_port() {
        return db_port;
    }

    /**
     * Obtiene la config inicial del servidor y bbdd
     * @return configuracion
     * @throws FileNotFoundException si no se puede abrir o leer el archivo
     */
    public static NetworkConfig initialConfig() throws FileNotFoundException {
        Gson gson = new Gson();
        JsonReader jsonReader = new JsonReader(new FileReader("config.json"));
        NetworkConfig networkConfig = gson.fromJson(jsonReader, NetworkConfig.class);
        return networkConfig;
    }
}
