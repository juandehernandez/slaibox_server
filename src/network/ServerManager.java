package network;

import controller.ServerViewController;
import model.AESCrypt;
import model.Database;
import network.packet.*;
import network.packet.request.Login;
import network.packet.request.Register;
import network.packet.response.UserAddedToChannel;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

/**
 * Clase que gestiona las conexiones de los usuarios, y cuando se conecta un nuevo usuario se le asigna un
 * trabajador que se encarga unicamente de el
 *
 * @author Daniel
 * @version 1.1.0
 */
public class ServerManager extends Thread {
    public static final int TIMEOUT = 5000;
    private static final int PORT = 3344;
    static ServerManager instance;
    private ServerSocket server;
    private final List<ClientWorker> clients;
    private Database db;
    private ServerViewController controller;

    /**
     * Mapa con los Profile de todos los usuarios registrados,
     * la key es el email del usuario y el valor el Profile
     */
    private final Map<String, Profile> registeredUsers;

    /**
     * Mapa con todos los Channels del sistemaa, la key es la id del canal
     * y el valor el Channel. Estos canales no tienen los mensajes, solo
     * el numero de mensajes de texto y ficheros y de que usuarios tienen
     */
    private final Map<Integer, Channel> currentChannels;

    /**
     * Constructor de la clase
     * @param controller controlador de la vista del servidor
     * @throws IOException no se ha podido crear el socket del servidor
     */
    private ServerManager(ServerViewController controller) throws IOException {
        this.controller = controller;
        server = new ServerSocket(controller.getPort_server());
        clients = Collections.synchronizedList(new ArrayList<>());
        server.setSoTimeout(TIMEOUT);
        db = new Database(controller.getDb_user(),controller.getDb_pass(), controller.getDb_name(),
                                            controller.getDb_ip() , controller.getDb_port());

        /* Esto llena el mapa de usuarios registrados y los muestra por la vista */

        registeredUsers = db.getRegisteredUsers();
        currentChannels = db.getAllChannelsInfo();
        /* Esto llena el mapa de los canales y los muestra por la vista*/
        inicialitzaCanals();
        inicialitzaUsuaris();
        start();
    }

    /**
     * Getter de la instancia del servidor del patron Singletone
     * @param controller controlador de la vista del servidor
     * @return instancia del ServerManager
     */
    public static ServerManager getInstance(ServerViewController controller) {
        if (instance == null) {
            try {
                instance = new ServerManager(controller);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    /**
     * Thread de esperar conexiones de clientes
     */
    @Override
    public void run() {
        while (!server.isClosed()) {
            try {
                //Esperem connexió del client
                Socket sClient = server.accept();
                clients.add(new ClientWorker(this,sClient));
            } catch (IOException e) {
                //e.printStackTrace();
            }
        }
    }

    /**
     * Hace la peticion a la base de datos de obtener todos los usuarios
     * y los muestra por la vista
     */
    private void inicialitzaUsuaris() {
        synchronized (registeredUsers) {
            registeredUsers.forEach((k, v) -> controller.addUserToView(v));
        }
    }

    /**
     * Hace la peticion a la base de datos de obtener todos los canales
     * y los muestra por la vista
     */
    private void inicialitzaCanals() {
        synchronized (currentChannels) {
            currentChannels.forEach((k, v) -> controller.addChannelToView(v));
        }
    }

    public List<Message> getMessagesFromChannel(int channelId, int offset) {
        return db.getMessagesFromChannel(channelId, offset);
    }


    /**
     * Para el servidor, cerrando la database y las conexiones con los clientes
     * de manera amigable. Cierra el socket del servidor haciendo que su thread
     * pare...
     */
    public synchronized void stopServer() {
        synchronized (clients) {
            for ( int i = 0; i< clients.size(); i++){
                clients.get(i).stopClient();
            }
        }
        db.close();
        try {
            server.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Closing server...");
    }

    /**
     * Envia un mensaje de la clase Message a todos los usuarios a los que
     * les pueda interesar
     * @param message mensaje a enviar
     */
    private void sendMessageMulticast(Message message) {
            for(ClientWorker c : clients) {
                if(c.belongsToChannel(message.getIdChannel())) {
                    if (!c.userIsOnChannel(message.getIdChannel())) {
                        Packet p = new MessageUnread(currentChannels.get(message.getIdChannel()).getName(), message.getIdChannel());
                        c.sendPacketToUser(p);
                        c.sendPacketToUser(message);
                    } else {
                        c.sendPacketToUser(message);
                    }
                }
            }

    }

    /**
     * Envia un paquete de la clase Packet a todos los usuarios a los que les
     * pueda interesar
     * @param packet paquete a enviar
     * @param channelId id del canal destino
     */
    protected void sendPacketMulticast(Packet packet, int channelId) {

            for(ClientWorker c : clients) {
                if(c.belongsToChannel(channelId)) {
                    c.sendPacketToUser(packet);
                }
            }

    }

    /**
     * Añade un usuario a un canal en la base de datos, si es satisfactorio
     * hace los cambios oportunos en la vista y en los dos mapas de usuarios y
     * canales de esta clase. Tambien notifica a los clientes oportunos de este
     * cambio
     * @param userEmail email del usuario
     * @param channelId id del canal
     * @return -1 si NO se ha podido añadir y otro numero si SI se ha hecho
     */
    public int insertUserInChannel(String userEmail, int channelId) {
        int result = db.insertUserChannel(userEmail, channelId);
        if (result != -1) {
            Profile profile = registeredUsers.get(userEmail);
            Channel channel = currentChannels.get(channelId);
            profile.getIdChannels().add(channelId);
            channel.getUsers().add(userEmail);
            controller.updateUser(profile);
            controller.updateChannel(channel);
            if(!channel.isPrivate()) {
                List<Profile> profiles = new ArrayList<>();
                for (String e : channel.getUsers()) {
                    profiles.add(registeredUsers.get(e));
                }
                UserAddedToChannel packet = new UserAddedToChannel(profile, channel, profiles);
                sendPacketMulticast(packet, channel.getId());
            }
        }
        return result;
    }

    /**
     * Añade un nuevo canal a la base de datos. Si esto es satisfactorio hace los
     * cambios oportunos en la vista y en el mapa de canales de esta clase.
     * @param channelName nombre del canal
     * @param channelDescription descripcion del canal
     * @param isPrivate si el canal es privado o no
     * @return -1 si NO se ha podido añadir y la id del canal añadido si ha ido bien
     */
    public int addNewChannel(String channelName, String channelDescription, boolean isPrivate) {
        int result = db.insertChannel(channelName, channelDescription, isPrivate);
        synchronized (currentChannels) {
            for (Channel c : currentChannels.values()) {
                if(c.getName().equals(channelName)) return -1;
            }

            if (result != -1) {
                Channel channel = new Channel(channelName, channelDescription, new ArrayList<String>(),
                        isPrivate, result, null, 0, new ArrayList<File>(), 0);
                currentChannels.put(channel.getId(), channel);
                controller.addChannelToView(channel);
            }
        }
        return result;
    }

    /**
     * Elimina un canal de la base de datos. Eliminando todos sus mensajes y usuarios.
     * Luego se encargará de avisar al controlador para que actualize la vista y se elimine el canal de la tabla
     * @param channelId
     * @param channelName
     */
    public void deleteChannel(int channelId, String channelName) {
        if (db.deleteChannel(channelId) > 0) {
            ChannelDeleted packet = new ChannelDeleted(channelId, channelName);
            sendPacketMulticast(packet, channelId);

            currentChannels.remove(channelId);
            controller.deleteChannelToView(channelId);
        }
    }

    /**
     * Se llama a esta funcion cuando el cliente quiere terminar la connexion o
     * le ocurre algun error. Actualiza la vista y pone el status del usuario a
     * desconectado, se comprueva que no este eliminado por si acaso...
     * @param socket socket a cerrar
     */
    public void clientSocketClosed(ClientWorker socket) {
        Profile profile = socket.getProfile();

        if (profile != null){
            if (!profile.getStatus().equals(Profile.DELETED)) {
                registeredUsers.get(socket.getProfile().getEmail()).setStatus(Profile.DISCONNECTED);
                controller.updateUser(profile);
                sendBroadcast(new UserStatusChanged(profile.getEmail(), Profile.DISCONNECTED));
            }
        }

        clients.remove(socket);

    }

    /**
     * Se llama a esta funcion cuando se recibe un mensaje de texto de un cliente,
     * se añade en la base de datos y si es satisfactorio se hace un multicast
     * a los clientes a los que pueda interesar, se actualiza el numero de mensajes
     * del canal y se actualiza la vista
     * @param t mensaje de texto recibido
     * @return -1 si NO se ha podido añadir y otro numero si SI se ha hecho
     */
    public int textReceived(Text t) {
        int result = db.insertMessage(t.getEmailUser(), t.getIdChannel(), t.getDate(), t.getMessage()); //TODO PORQUE COÑO VOID SI ERA INT
        if (result != -1) {
            Channel c = currentChannels.get(t.getIdChannel());
            c.incrementTotalMessages();
            controller.updateChannel(c);
            t.setMessageId(result);
            sendMessageMulticast(t);
        }
        return result;
    }

    /**
     * Obtiene un file entero segun la id indicada
     * @param fileId id del file a obtener
     * @return file
     */
    public File getCompleteFile(int fileId) {
        return db.getCompleteFile(fileId);
    }

    /**
     * Se llama a esta funcion cuando se recibe un mensaje de archivo de un cliente,
     * se añade en la base de datos y si es satisfactorio se hace un multicast
     * a los clientes a los que pueda interesar, se actualiza el numero de mensajes
     * del canal y se actualiza la vista
     * @param f mensaje de archivo recibido
     * @return -1 si NO se ha podido añadir y otro numero si SI se ha hecho
     */
    public int fileReceived(File f) {
        int result = db.insertFile(f.getEmailUser(), f.getIdChannel(), f.getDate(), f.getData(), f.getName());
        if (result != -1) {
            Channel c = currentChannels.get(f.getIdChannel());
            c.incrementTotalMessages();
            c.getFilesInfo().add(new File(0, "NONE"));
            f.setMessageId(result);
            controller.updateChannel(c);
            sendMessageMulticast(f);
        }
        return result;
    }

    /**
     * Comprueva si la peticion de login es correcta
     * @param login paquete de la peticion de login
     * @return true si el login esta OK y false si no lo esta
     */
    public boolean isLoginCorrect(Login login) {
        return db.pwCoincide(login.getEmailUser(), login.getPassword());
    }

    /**
     * Devuelve un Profile a partir de un paquete de un cliente, este tiene
     * que haber hecho un login satisactiorio antes. Cuidado porque el
     * email de un paquete puede ser el nombre de usuario o paquete
     * @param p paquete
     * @return Profile del cliente del paquete
     */
    public Profile getProfile(Packet p) {
        String emailOrUsername = p.getEmailUser();
        String email;
        if(emailOrUsername.contains("@")) {
            email = emailOrUsername;
        }else{
            email = db.getEmailFromUsername(emailOrUsername);
        }
        return registeredUsers.get(email);
    }

    /**
     * Se llama a esta funcion cuando un usuario ha hecho login, se encarga
     * de actualizar su estado a conectado (comprueba si no esta eliminado
     * por si acaso...), actualiza su ultima conexion y actualzia la vista
     * @param p Profile del usuario conectado
     */
    public void userIsConnected(Profile p) {
            if (!p.getStatus().equals(Profile.DELETED)) {
                p.setStatus(Profile.CONNECTED);
                registeredUsers.put(p.getEmail(), p);
            }
            Profile aux = registeredUsers.get(p.getEmail());
            if (aux != null) {
                aux.setStatus(p.getStatus());
            }
            db.updateLastConnectionTime(p.getEmail(), new Date());
            controller.updateUser(p);
            sendBroadcast(new UserStatusChanged(p.getEmail(), p.getStatus()));
    }

    /**
     * Obtiene la inicializacion de datos a enviar a un cliente cuando se logea
     * correctamente
     * @param profile Profile del cliente logeado
     * @return InitData con toda la informacion necesaria para la inicializacion
     */
    public InitData getInitData(Profile profile) {
        HashMap<Integer, Channel> userChannels = db.getChannelsFromUser(profile.getEmail());
        HashMap<String, Profile> userRelatedProfiles = db.getUsersOnClientChannels(userChannels);

            userRelatedProfiles.forEach((k,v) -> {
                if (registeredUsers.get(k) != null) {
                    v.setStatus(registeredUsers.get(k).getStatus());
                }
            });

        return new InitData(userChannels, userRelatedProfiles, profile);
    }

    /**
     * Registra a un usuario nuevo en la base de datos, primero se comprueba
     * la contraseña y luego en la base de datos si su nombre y email
     * son unicos, si es satisfactorio se añade al mapa de esta clase y se actualiza
     * la vista
     * @param request request de registro recibida
     * @return true si se ha registrado correctament y false si no lo ha hecho
     */
    public int registerUser(Register request) {
        String pwd = AESCrypt.decrypt(request.getPassword());
        String pwdConfirm = AESCrypt.decrypt(request.getPasswordConfirm());

        if (!pwd.equals(pwdConfirm)) return -4;
        if (pwd.length() < 6) return -5;
        if (!hasNumber(pwd)) return -6;
        if (pwd.equals(pwd.toLowerCase())) return -7;
        if (pwd.equals(pwd.toUpperCase())) return -8;

        int result = db.registerUser(request);
        if (result == 1) {
            Profile p = db.getUserFromEmail(request.getEmail());
            registeredUsers.put(p.getEmail(), p);
            controller.addUserToView(p);
        }
        return result;
    }

    /**
     * Funcion para saber si una String contiene al menos un numero
     *
     * @param s String en cuestion
     * @return true si tiene al menos un numero, falso si no
     */
    private boolean hasNumber(String s) {
        if (s.isEmpty()) return false;
        for (char c : s.toCharArray()) {
            if (Character.isDigit(c)) return true;
        }
        return false;
    }

    /**
     * Banea o desbanea a un usuario segun su estado actual, tambien actualiza
     * la vista
     * @param email email del usuario a banear/unbanear
     */
    public void banUnbanUser(String email) {
        Profile p = registeredUsers.get(email);
        if (p.getStatus().equals(Profile.DELETED)) {
            p.setStatus(Profile.DISCONNECTED);
            db.setBannedStatus(p.getEmail(), false);
        } else  {
            p.setStatus(Profile.DELETED);
            db.setBannedStatus(p.getEmail(), true);
        }
        Profile aux = registeredUsers.get(p.getEmail());
        if (aux != null) {
            aux.setStatus(p.getStatus());
        }
        controller.updateUser(p);
        sendBroadcast(new UserStatusChanged(p.getEmail(), p.getStatus()));
    }

    /**
     * Actualiza el ultimo mensaje leido de un canal-usuario
     * @param userEmail email del usuario
     * @param channelId id del canal
     * @param messageId id del ultimo mensaje leido en un canal por un usuario
     */
    public void updateLastViewedMessage(String userEmail, int channelId, int messageId) {
        db.updateLastMessageRead(userEmail, channelId, messageId);
    }

    /**
     * Envia un packet a todos los clientes actuales excepto al que pertenece el packet
     * @param p packet a enviar por broadcast
     */
    public void sendBroadcast(Packet p) {
        int size = clients.size();
        for(int i = 0; i < size; ++i) {
            if (clients.get(i).getProfile() != null && !clients.get(i).getProfile().getEmail().equals(p.getEmailUser())) {
                clients.get(i).sendPacketToUser(p);
            }
        }
    }

    /**
     * Getter de la database
     * @return database
     */
    public Database getDatabase() {
        return db;
    }

    /**
     * Devuelve todos los canales existentes
     * @return mapa
     */
    public Map<Integer, Channel> getCurrentChannels(){
        return currentChannels;
    }

    /**
     * Getter del perfil de un usuario a partir de su email
     * @param email email
     * @return perfil asociado al email
     */
    public Profile getRegisteredUser(String email){
        return registeredUsers.get(email);
    }

    /**
     * Getter del controlador de la vista del servidor
     * @return controlador de la vista del servidor
     */
    public ServerViewController getController() {
        return controller;
    }
}
