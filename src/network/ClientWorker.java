package network;

import network.packet.*;
import network.packet.request.*;
import network.packet.response.FileRequestResponse;
import network.packet.response.MessagesRequestResponse;
import network.packet.response.Response;
import network.packet.response.UserAddedToChannel;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que se encarga de trabajar con un unico cliente
 *
 * @author Daniel
 * @version 1.1.0
 */
public class ClientWorker extends Thread {

    private ServerManager serverManager;
    private Socket s;
    private final ObjectInputStream in;
    private final ObjectOutputStream out;
    private int errors;
    private boolean running;
    private int currentChannelId;

    /**
     * Profile del actual usuario del socket, null si no se ha logeado
     * correctamente
     */
    private Profile profile;

    /**
     * Constructor del ClientWorker
     * @param serverManager instancia del ServerManager
     * @param s socket del cliente
     * @throws IOException si no se puede crear los canales del socket o utilizarlos
     */
    public ClientWorker(ServerManager serverManager, Socket s) throws IOException {
        this.s = s;
        this.serverManager = serverManager;
        s.setSoTimeout(ServerManager.TIMEOUT);
        in = new ObjectInputStream(s.getInputStream());
        out = new ObjectOutputStream(s.getOutputStream());
        profile = null;
        errors = 0;
        running = true;
        currentChannelId = -1;
        start();
        out.writeObject(new Response("OK"));
    }

    /**
     * Thread de comunicacion con el cliente
     */
    @Override
    public void run() {
        while (running) {
            Packet p;
            try {
                p = (Packet) in.readObject();

                /* Tratar el paquete segun su subclase */
                if (p instanceof Login) {
                    loginRequestReceived((Login) p);
                } else if (p instanceof Register) {
                    registerRequestReceived((Register) p);
                } else if (p instanceof File) {
                    fileReceived((File) p);
                } else if (p instanceof Text) {
                    textReceived((Text) p);
                } else if (p instanceof Logout) {
                    logoutRequestReceived((Logout) p);
                } else if (p instanceof MessagesRequest) {
                    messagesRequestReceived((MessagesRequest) p);
                } else if (p instanceof CreatePrivateChannelRequest) {
                    createPrivateChannelRequestReceived((CreatePrivateChannelRequest) p);
                } else if (p instanceof UserChangedCurrentChannel) {
                    userChangedCurrentChannel((UserChangedCurrentChannel) p);
                }else if(p instanceof FileRequest){
                    fileRequested((FileRequest) p);
                } else if(p instanceof LastView) {
                    serverManager.getDatabase().updateLastMessageRead(p.getEmailUser(),  ((LastView) p).getIdChannel(),
                            ((LastView) p).getIdLastMessage());
                } else if (p instanceof UserStatusChanged) {
                    userChangedStatus((UserStatusChanged) p);
                }
                errors = 0;
            } catch (SocketException e){
                try {
                    serverManager.clientSocketClosed(this);
                    s.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                running = false;
            }catch (IOException | ClassNotFoundException e) {
                if (!(e instanceof EOFException) && !(e instanceof SocketTimeoutException))
                    e.printStackTrace();
            }
        }
    }

    /**
     * Se encarga de tratar el cambio de estado de un usuario
     *
     * @param p peticion
     */
    private void userChangedStatus(UserStatusChanged p) {
        serverManager.sendBroadcast(p);
        serverManager.getProfile(p).setStatus(p.getStatus());
        serverManager.getController().updateUser(serverManager.getProfile(p));
    }

    /**
     * trata la peticion de un archivo
     */
    private void fileRequested(FileRequest fr) {
        sendPacketToUser(new FileRequestResponse(serverManager.getCompleteFile(fr.getFileId())));
    }

    /**
     * Notifica de que el cliente ha cambiado el canal que esta visualziando
     * @param p packet que informa sobre este cambio
     */
    private void userChangedCurrentChannel(UserChangedCurrentChannel p) {
        currentChannelId = p.getNewCurrentChannel();
    }


    /**
     * Trata la peticion de login recibida, comprueba si los datos son correctos,
     * y informa al cliente de ello, si son correctos le envia los datos de
     * inicializacion
     * @param request peticion de login
     * @throws IOException no se puede comunicar con el cliente
     */
    private void loginRequestReceived(Login request) throws IOException {

        if (serverManager.isLoginCorrect(request)) {
            profile = serverManager.getProfile(request);
            if (profile != null) {
                if (profile.getStatus().equals(Profile.DELETED)) {
                    sendPacketToUser(new Response("Estás baneado! No puedes continuar.", request));
                    return;
                }
                serverManager.userIsConnected(profile);
                Response response = new Response("OK", request);
                sendPacketToUser(response);
                InitData initData = serverManager.getInitData(profile);
                sendPacketToUser(initData);
            } else {
                Response response = new Response("Usuario y/o contraseña son incorrectos!", request);
                sendPacketToUser(response);
            }
        } else {
            Response response = new Response("Usuario y/o contraseña son incorrectos!", request);
            sendPacketToUser(response);
        }

    }

    /**
     * Trata la peticion de registro recibida, informa al cliente segun
     * sea el resultado que devuelve el servidor de esta peticion de registro
     * @param request peticion de registro
     * @throws IOException si la comunicacion con el cliente no va
     */
    private void registerRequestReceived(Register request) throws IOException {
        Response response;
        switch (serverManager.registerUser(request)) {
            case 1:
                response = new Response("Registrado correctamente", request);
                break;

            case -1:
                response = new Response("El usuario o el correo ya existen!", request);
                break;

            case -4:
                response = new Response("Las contraseñas no concuerdan!", request);
                break;

            case -5:
                response = new Response("La contraseña no tiene el mínimo número de caracteres necesarios!", request);
                break;
            case -6:
                response = new Response("La contraseña no es alfanumerica!", request);
                break;
            case -7:
                response = new Response("La contraseña no puede sólo contener minusculas!", request);
                break;
            case -8:
                response = new Response("La contraseña no puede sólo contener mayusculas!", request);
                break;

            case 0:
            case -2:
            case -3:
            default:
                response = new Response("Ha habido un error al registrarte, intentalo nuevamente luego!", request);
                break;
        }
        sendPacketToUser(response);
    }

    /**
     * Trata un mensaje de texto recibida, se la pasa al servidor para que
     * la gestione
     * @param text mensaje de texto
     */
    private void textReceived(Text text) {
        serverManager.textReceived(text);
    }

    /**
     * Trata un mensaje de texto recibida, se la pasa al servidor para que
     * la gestione
     * @param file archivo recibido
     */
    private void fileReceived(File file) {
        serverManager.fileReceived(file);
    }

    /**
     * Trata una peticion de logout recibida, avisa al servidor que vas
     * a cerrar la conexion y cierra el socket del cliente
     * @param request peticion de logout
     * @throws IOException si la comunicacion con el cliente no va
     */
    private void logoutRequestReceived(Logout request) throws IOException {
        request.getChannels().forEach(
                c -> serverManager
                        .updateLastViewedMessage(request.getEmailUser(), c.getId(), c.getLastViewedMessageId()));
        running = false;
        serverManager.clientSocketClosed(this);
        s.close();
    }

    /**
     * Trata una peticion de solicitiud de mensajes, obtiene los mensajes del
     * servidor y devuelve una respuesta a esta peticion con los mensajes
     * @param request peticion de solicitud de mensajes
     * @throws IOException si la comunicacion con el cliente no va
     */
    private void messagesRequestReceived(MessagesRequest request) throws  IOException {
        List<Message> messages = serverManager
                                    .getMessagesFromChannel(request.getChannelId(),
                                                            request.getOffset());
        sendPacketToUser(new MessagesRequestResponse(messages,
                request.getChannelName(),
                request.getChannelId(),
                request.getOffset()));
    }

    /**
     * Maneja cuando se recibe la petitcion de crear un canal privado
     * @param request
     */
    private void createPrivateChannelRequestReceived(CreatePrivateChannelRequest request) {
        String channelName = request.getEmailUser() + request.getOtherEmail();
        int result = serverManager.addNewChannel(channelName,
                "A private channel", true);
        if (result != -1) {
            serverManager.insertUserInChannel(request.getEmailUser(), result);
            serverManager.insertUserInChannel(request.getOtherEmail(), result);
            Profile profile1 = serverManager.getRegisteredUser(request.getEmailUser());
            Profile profile2 = serverManager.getRegisteredUser(request.getOtherEmail());
            Channel channel = serverManager.getCurrentChannels().get(result);
            List<Profile> profiles = new ArrayList<>();
            profiles.add(profile1);
            profiles.add(profile2);
            UserAddedToChannel packet = new UserAddedToChannel(profile1, channel, profiles);
            serverManager.sendPacketMulticast(packet, channel.getId());
            packet = new UserAddedToChannel(profile2, channel, profiles);
            serverManager.sendPacketMulticast(packet, channel.getId());
        }
    }

    /**
     * Comprueba si el usuario de este socket pertenece al canal indicado
     * por la id recibida
     * @param idChannel id del canal
     * @return true si el usuario(socket) pertenece al canal o false si no
     */
    public boolean belongsToChannel(int idChannel) {
        if (profile == null) return false;
        for (int id : profile.getIdChannels()) {
            if(id == idChannel) {
                return true;
            }
        }
        return false;
    }

    /**
     * Envia un paquete al cliente
     * @param packet paquete a enviar
     */
    public void sendPacketToUser(Packet packet) {
        synchronized (out) {
            try {
                out.writeObject(packet);
            }catch (SocketException e){
                System.out.println(packet.getEmailUser() + " " + packet.getClass() + " " + profile.getEmail());
            }catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Cierra el socket del servidor, haciendo que el bucle del thread pare..
     */
    public void stopClient() {
            try {
                serverManager.clientSocketClosed(this);
                if(profile != null) {
                    sendPacketToUser(new Logout(profile.getEmail()));
                }else{
                    sendPacketToUser(new Logout());
                }
                running = false;
                s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    /**
     * Retorna si el usuario esta actualmente en el canal especificado
     * @param channelId id del canal
     * @return si esta o no
     */
    public boolean userIsOnChannel(int channelId) {
        return currentChannelId == channelId;
    }

    /**
     * Obtiene el Profile del usuario actual de este socket, null si no hay ningun
     * usuario logeado correctamente
     * @return Profile del socket
     */
    public Profile getProfile() {
        return  profile;
    }
}
