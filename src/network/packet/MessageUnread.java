package network.packet;

/**
 * Dice que mensaje no se ha leido
 *
 * @author Sergi
 */
public class MessageUnread extends Packet {
    private String channelName;
    private int channelId;

    /**
     * Constructor de la clase
     * @param channelName nombre del canal
     * @param channelId id del canal
     */
    public MessageUnread(String channelName, int channelId) {
        this.channelId = channelId;
        this.channelName = channelName;
    }

    /**
     * Devuelve el nombre del canal
     * @return
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * Devuelve el id del canal
     * @return
     */
    public int getChannelId() {
        return channelId;
    }
}
