package network.packet.request;

import model.AESCrypt;
import network.packet.Packet;

/**
 * Clase que representa la peticion de registro de un usuario
 *
 * @author Daniel
 * @version 1.2.0
 */
public class Register extends Packet {

    private final String email;
    private String pwd;
    private String pwdConfirm;

    /**
     * Constructor
     * @param user nombre de usuario
     * @param email email del usuario
     * @param password contrasena del usuario
     * @param passwordConfirm confirmacion de contrasena del usuario
     */
    public Register(String user, String email, char[] password, char[] passwordConfirm) {
        super(user);
        this.email = email;
        pwd = AESCrypt.encrypt(password);
        pwdConfirm = AESCrypt.encrypt(passwordConfirm);
    }

    /**
     * Getter del email del usuario que hace la peticion
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Getter de la contrasena del usuario
     * @return contrasena
     */
    public String getPassword() {
        return pwd;
    }

    /**
     * Getter de la confirmacion de contrasena del usuario
     * @return confirmacion de la contrasena
     */
    public String getPasswordConfirm() {
        return pwdConfirm;
    }
}
