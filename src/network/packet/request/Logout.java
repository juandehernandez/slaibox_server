package network.packet.request;

import network.packet.Channel;
import network.packet.Packet;

import java.util.ArrayList;

/**
 * Clase que representa que el usuario se quiere desconectar
 *
 * @author Daniel
 * @version 1.0.0
 */
public class Logout extends Packet {
    /**
     * Estos canales solo contienen el nombre del canal, su id y la id del ultimo
     * mensaje leido en cada canal
     */
    private ArrayList<Channel> channels;

    /**
     * Constructor
     */
    public Logout(){
        super();
    }

    /**
     * Constructor
     * @param user email del usuario que hace la peticion
     */
    public Logout(String user) {
        super(user);
        this.channels = null;
    }

    /**
     * Constructor
     * @param user email del usuario que hace la peticion
     * @param channels contiene el ultimo mensaje leido de cada canal del usuario
     */
    public Logout(String user, ArrayList<Channel> channels) {
        super(user);
        this.channels = channels;
    }

    /**
     * Getter de los canales
     * @return lista de canales
     */
    public ArrayList<Channel> getChannels() {
        return channels;
    }
}
