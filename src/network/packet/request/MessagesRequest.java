package network.packet.request;

import network.packet.Packet;

/**
 * Clase para pedir mensajes de un canal
 *
 * @author Sergi
 */
public class MessagesRequest extends Packet {
    private int channelId;
    private int offset;
    private String channelName;

    /**
     * Constructor
     * @param channelName nombre del canal del que se piden los mensajes
     * @param channelId id del canal del que se piden los mensajes
     * @param offset offset de que mensajes pedir del canal
     */
    public MessagesRequest (String channelName, int channelId, int offset) {
        super();
        this.channelName = channelName;
        this.channelId = channelId;
        this.offset = offset;
    }

    /**
     * Getter del nombre del canal de la peticion
     * @return nombre del canal de la peticion
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * Getter de la id del canal de la peticion
     * @return id del canal de la peticion
     */
    public int getChannelId() {
        return channelId;
    }

    /**
     * Getter del offset de los mensajes a obtener
     * @return offset de los mensajes a obtener
     */
    public int getOffset() {
        return offset;
    }
}
