package network.packet;

/**
 * Actualiza el ultimo mensaje no leido
 *
 * @author Miguel
 */
public class LastView extends Packet {

    private int idLastMessage;
    private int idChannel;

    /**
     * Constructor de la clase
     * @param email email
     * @param idLastMessage id del ultimo mensaje visto
     * @param idChannel id del canal
     */
    public LastView(String email, int idLastMessage, int idChannel) {
        super(email);
        this.idLastMessage = idLastMessage;
        this.idChannel = idChannel;
    }

    /**
     * Getter de la id del ultimo mensaje visto
     * @return id del ultimo mensaje visto
     */
    public int getIdLastMessage() {
        return idLastMessage;
    }

    /**
     * Setter de la id del ultimo mensaje visto
     * @param idLastMessage id del ultimo mensaje visto
     */
    public void setIdLastMessage(int idLastMessage) {
        this.idLastMessage = idLastMessage;
    }

    /**
     * Getter de la id del canal
     * @return id del canal
     */
    public int getIdChannel() {
        return idChannel;
    }

    /**
     * Setter de la id del canal
     * @param idChannel id del canal
     */
    public void setIdChannel(int idChannel) {
        this.idChannel = idChannel;
    }

}

