package network.packet;


import java.util.HashMap;

/**
 * Clase que se encarga de inicializar toda la información del perfil de un usuario, los canales a los
 * cuales está ese usuario, tanto privados como públicos. Los datos se obtenien de la base de datos.
 *
 * @author Miguel Abellán
 * @author Sergi Valbuena
 * @version 2.0
 */
public class InitData extends Packet {

    private Profile profile;
    private HashMap<Integer, Channel> channels;
    private HashMap<String, Profile> users;

    /**
     * Constructor de la clase
     * @param channels todos los canales en los que se encuentra el usuario
     * @param users de los canales en los que esta el usuario
     * @param profile perfil del usuario
     */
    public InitData(HashMap<Integer, Channel> channels, HashMap<String, Profile> users,Profile profile) {
        this.channels = channels;
        this.users = users;
        this.profile = profile;
    }

    /**
     * Devuelve los canales
     * @return canales
     */
    public HashMap<Integer, Channel> getChannels() {
        return channels;
    }

    /**
     * Devuelve los usuarios
     * @return usuarios
     */
    public HashMap<String, Profile> getUsers() {
        return users;
    }

    /**
     * Devuelve el perfil del usuario
     * @return perfil del usuario
     */
    public Profile getProfile() {
        return profile;
    }
}
