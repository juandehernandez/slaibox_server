package network.packet;

import util.FileUtils;

/**
 * Clase para enviar un fichero cualquiera.
 *
 * @author Daniel Ortiz
 * @version 1.2.0
 */
public class File extends Message {

    private final byte[] data;
    private final String name;
    private final String ext;

    /**
     * Constructor de la clase
     * @param user usuario que envia el archivo
     * @param data datos del archvio
     * @param name nombre del archivo
     * @param idChannel id del canal al que pertenece el archivo
     * @param messageId id del mensaje del archivo
     */
    public File(String user, byte[] data, String name, int idChannel, int messageId) {
        super(user, idChannel, messageId);
        this.data = data;
        this.name = name;
        ext = FileUtils.getExtension(name);
    }

    /**
     * Constructor de la clase
     * @param user usuario que envia el archivo
     * @param data datos del archvio
     * @param name nombre del archivo
     * @param idChannel id del canal al que pertenece el archivo
     */
    public File(String user, byte[] data, String name, int idChannel) {
        super(user, idChannel);
        this.data = data;
        this.name = name;
        ext = FileUtils.getExtension(name);
    }

    /**
     * Constructor de la clase
     * @param messageId id del mensaje del archivo
     * @param name nombre del archivo
     */
    public File(int messageId, String name) {
        super(messageId);
        this.name = name;
        this.data = null;
        ext = FileUtils.getExtension(name);
    }

    /**
     * Constructor de la clase
     * @param messageId id del mensaje del archivo
     * @param name nombre del archivo
     * @param data datos del archvio
     */
    public File(int messageId, String name, byte[] data) {
        super(messageId);
        this.name = name;
        this.data = data;
        ext = FileUtils.getExtension(name);
    }

    /**
     * Devuelve los datos del archivo
     * @return datos del archivo
     */
    public byte[] getData() {
        return data;
    }

    /**
     * Devuelve el nombre del archivo
     * @return nombre del archivo
     */
    public String getName() {
        return name;
    }

    /**
     * Devuelve la extencion del archivo
     * @return extension del archvio
     */
    public String getExt() {
        return ext;
    }
}
