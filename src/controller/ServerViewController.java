package controller;

import model.Database;
import model.config.NetworkConfig;
import network.ServerManager;
import network.packet.Channel;
import network.packet.Profile;
import vista.ChannelsTabView;
import vista.MainView;
import vista.StatisticsTabView;
import vista.UsersTabView;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;

/**
 * Controlador prinicipal de la vista del servidor
 */
public class ServerViewController extends WindowAdapter implements ActionListener, ListSelectionListener {
    /** Vista principal, dentro tiene un Tabbed Pane */
    private MainView mainView;

    /** Tab de los usuarios */
    private UsersTabView usersTabView;
    /** Tab de los canales */
    private ChannelsTabView channelsTabView;
    /** Tab de las estadisticas */
    private StatisticsTabView statisticsTabView;

    /** Instancia del servidor */
    private ServerManager serverManager;

    /** configuracion de la coneccion */
    private NetworkConfig nConfig;

    /** Instancia de la base de datos*/

    /**
     * Constructor del controlador del servidor
     * @param mainView vista principal
     */
    public ServerViewController(MainView mainView) {
        this.mainView = mainView;
        usersTabView = mainView.getUsersTabView();
        channelsTabView = mainView.getChannelsTabView();
        statisticsTabView = mainView.getStatisticsTabView();
        serverManager = ServerManager.getInstance(this);
    }

    /**
     * Constructor del controlador del servidor
     * @param mainView vista principal
     */
    public ServerViewController(MainView mainView, NetworkConfig nConfig) {
        this.mainView = mainView;
        this.nConfig = nConfig;
        usersTabView = mainView.getUsersTabView();
        channelsTabView = mainView.getChannelsTabView();
        statisticsTabView = mainView.getStatisticsTabView();
        serverManager = ServerManager.getInstance(this);
    }


    /**
     * Escuchamos las acciones de la vista principal (de todos los Tabs)
     * @param event
     */
    @Override
    public void actionPerformed(ActionEvent event) {
        int channelId;
        switch (event.getActionCommand()) {

            /* TAB DE LOS USUARIOS **/

            /* Boton de ban/unban usuario pulsado **/
            case UsersTabView.BAN_UNBAN_USER_BUTTON:
                String email = usersTabView.getSelectedEmail();
                /* Si el email es null significa que no hay ninguna fila (canal)
                 * seleccionado en su tabla
                 */
                if (email != null) {
                    serverManager.banUnbanUser(email);
                } else {
                    usersTabView.showError("No user is selected!", "Please select a user before un|banning");
                }
                break;

            /* TAB DE LOS CANALES */

            /* Boton de añadir un nuevo canal pulsado */
            case ChannelsTabView.ADD_CHANNEL_BUTTON:
                /* Se muestra el dialog para añadir la informacion del canal a añadir */
                String[] newChannelInfo = channelsTabView.showAddChannelDialog();
                /* Si es null significa que han cancelado el dialog */
                if (newChannelInfo != null) {
                    String newChannelName = newChannelInfo[0];
                    String newChannelDescription = newChannelInfo[1];
                    /* Se comprueva que se haya rellenado los dos campos (nombre y descripcion */
                    if (newChannelName != null && newChannelDescription != null) {
                        if(!newChannelName.contains("@")) {
                            int addNewCanal = serverManager.addNewChannel(newChannelName, newChannelDescription, false);
                            if(addNewCanal == -1) {
                                channelsTabView.showError("Error al crear canal", "No se pueden crear canales el mismo nombre");
                            }
                        } else {
                            channelsTabView.showError("Error en el nombre del canal","No se pueden crear canales con @ en el nombre");

                        }
                    } else {
                        channelsTabView.showError("Name or description is empty!", "Both fields must be filled before adding a channel");
                    }
                }
                break;

            /* Boton de añadir un usuario a un canal pulsado */
            case ChannelsTabView.ADD_USER_TO_CHANNEL_BUTTON:

                channelId = channelsTabView.getSelectedChannelId();
                /* Si la id es -1 significa que no hay ninguna fila (usuario) seleccionado
                 * en su tabla
                 */
                if (channelId != -1) {
                    /* Comprobar que el canal no sea privado */
                    Map<Integer, Channel> channels =  serverManager.getCurrentChannels();
                    if (!channels.get(channelId).isPrivate()) {

                        String userEmail = channelsTabView.showAddUserToChannelDialog();
                    /* Si el userEmail es null significa que no han rellenado el campo
                     * de email
                     */
                        if (userEmail != null) {
                        /* Comprobamos si se puede añadir el usuario al canal,
                         * si da error (-1) es porque el usuario no existe
                         */
                            if (serverManager.insertUserInChannel(userEmail, channelId) == -1) {
                                channelsTabView.showError("Couldn't add data to database", "The user doesn't exists or is already on the cannel");
                            }
                        } else {
                            channelsTabView.showError("Email is empty!", "Please add a correct email");
                        }
                    }else{
                        channelsTabView.showError("Private Channel", "You can't add users to a private channel");
                    }
                } else {
                    channelsTabView.showError("No channel is selected!", "Please select a channel before adding a user to it");
                }
                break;

            /* Boton de eliminar un canal pulsado */
            case ChannelsTabView.DELETE_CHANNEL_BUTTON:
                channelId = channelsTabView.getSelectedChannelId();
                String channelName = channelsTabView.getSelectedChannelName();

                serverManager.deleteChannel(channelId, channelName);
                break;
            case ChannelsTabView.STATICS_BUTTON:
                mainView.showStatisticsTab();
                break;
        }
    }

    /**
     * Actualiza la informacion de un usuario de la vista, si este no se encuentra
     * se añade al final de su tabla
     * @param p Profile a actualizar
     */
    public void updateUser(Profile p) {
        usersTabView.updateUser(p.getEmail(), p.getUsername(), p.getStatus(), p.getIdChannels().size(),
                p.getLastConnectionDate(), p.getRegistrationDate());
    }

    /**
     * Añade un nuevo usuario a la tabla de usuarios de la vista del servidor
     * @param p Profile a añadir
     */
    public void addUserToView(Profile p) {
        usersTabView.addUserToView(p.getEmail(), p.getUsername(), p.getStatus(), p.getIdChannels().size(),
                p.getLastConnectionDate(), p.getRegistrationDate());
    }

    /**
     * Actualiza la informacion de un canal de la vista, si este no se encuentra
     * se añade al final de su tabla
     * @param channel Channel a añadir
     */
    public void updateChannel(Channel channel) {
        channelsTabView.updateChannel(channel.getId(), channel.getName(), channel.getDescription(),
                channel.getUsers().size(), channel.getTotalMessagesNumber(),
                channel.getFilesInfo().size(), channel.isPrivate());

    }

    /**
     * Añade un nuevo canal a la tabla de canales de la vista del servidor
     * @param channel
     */
    public void addChannelToView(Channel channel) {


        channelsTabView.addChannelToView(channel.getId(), channel.getName(), channel.getDescription(),
                channel.getUsers().size(), channel.getTotalMessagesNumber(),
                channel.getFilesInfo().size(), channel.isPrivate());
    }

    /**
     * Elimina un canal de la tabla de canales de la vista del servidor
     * @param channelId id del canal a borrar
     */
    public void deleteChannelToView(int channelId) {
        channelsTabView.deleteChannelToView(channelId);
    }


    /**
     * devuelve el puerto usado para conectarse con el cliente
     * @return
     */
    public int getPort_server() {
        return nConfig.getPort_server();
    }

    /**
     * devuelve el nombre de la base de datos a la cual hay que conectarse
     * @return
     */
    public String getDb_name() {
        return nConfig.getDb_name();
    }

    /**
     * devuelve el usuario de la base de datos a la cual hay que conectarse
     * @return
     */
    public String getDb_user() {
        return nConfig.getDb_user();
    }

    /**
     * devuelve la contraseña del usuariro de la base de datos a la cual hay que conectarse
     * @return
     */
    public String getDb_pass() {
        return nConfig.getDb_pass();
    }

    /**
     * devuelve la ip de la base de datos a la cual hay que conectarse
     * @return
     */
    public String getDb_ip() {
        return nConfig.getDb_ip();
    }

    /**
     * puerto usado para conectarse con la base de datos
     * @return
     */
    public int getDb_port() {
        return nConfig.getDb_port();
    }



    /**
     * Cuando se cierra la ventana avisamos al ServerManager para que cierre todas
     * las comunicaciones bien
     * @param e evento
     */
    @Override
    public void windowClosing(WindowEvent e) {
        serverManager.stopServer();
    }

    /**
     * Detecta cuando se selecciona un canal en la channel tab
     * @param e evento
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        //Thread porque estas queries tardan la vida
        new Thread(() -> {

            int id = channelsTabView.getSelectedChannelId();
            Database db = serverManager.getDatabase();
            statisticsTabView.setChannelName(statisticsTabView.getChannelName());
            statisticsTabView.setMessagesNumber(db.getCountTextsLastWeek(id));
            statisticsTabView.setFileNumber(db.getCountFilesLastWeek(id));
            statisticsTabView.setImagesNumber(db.getCountPicturesLastWeek(id));
        }).start();
    }
}
