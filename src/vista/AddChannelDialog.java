package vista;

import javax.swing.*;
import java.awt.*;

/**
 * JPanel auxiliar para mostar un dialog personalizado para pedir
 * la informacion de un nuevo canal (su nombre y descricpion)
 */
public class AddChannelDialog extends JPanel {
    /** JTextField del nombre del canal */
    private final JTextField channelNameTF;
    /** JTextField de la descripcion del canal */
    private final JTextField channelDescriptionTF;

    /**
     * Constructor del dialog de añadir un nuevo canal
     */
    public AddChannelDialog() {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        channelNameTF = new JTextField();
        channelDescriptionTF = new JTextField();

        JPanel namePanel = new JPanel(new GridLayout());
        namePanel.add(new JLabel("Channel name:"));
        namePanel.add(channelNameTF);
        add(namePanel);

        JPanel descPanel = new JPanel(new GridLayout());
        descPanel.add(new JLabel("Channel description:"));
        descPanel.add(channelDescriptionTF);
        add(descPanel);

        setSize(300, 400);
    }

    /**
     * Devuelve el nombre del canal escrito
     * @return String, nombre del canal
     */
    public String getChannelName() {
        /* Si el campo esta vacio se devuelve null */
        if (channelNameTF.getText().isEmpty()) {
            return null;
        } else {
            return channelNameTF.getText();
        }
    }

    /**
     * Devuelve la descripcion del canal escrita
     * @return String, descripcion del canal
     */
    public String getChanelDescription() {
        /* Si el campo esta vacio se devuelve null */
        if (channelDescriptionTF.getText().isEmpty()) {
            return null;
        } else {
            return channelDescriptionTF.getText();
        }
    }
}
