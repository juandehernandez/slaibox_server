package vista;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Panel donde esta la informacion de los canales en una tabla y con los
 * botones para hacer las acciones diponibles con ellos
 */
public class ChannelsTabView extends JPanel {

    /** Constantes para los ActionCommand de los eventos de los botones */
    public static final String ADD_CHANNEL_BUTTON = "ADD_CHANNEL_BUTTON";
    public static final String ADD_USER_TO_CHANNEL_BUTTON = "ADD_USER_TO_CHANNEL_BUTTON";
    public static final String DELETE_CHANNEL_BUTTON =  "DELETE_CHANNEL_BUTTON";
    public static final String STATICS_BUTTON =  "STATICS_BUTTON";


    /** Constantes para identificar las columnas de la tabla de canales */
    private static final int CHANNEL_ID = 0;
    private static final int CHANNEL_NAME = 1;
    private static final int CHANNEL_DESCRIPTION = 2;
    private static final int CHANNEL_USERS_NUMBER = 3;
    private static final int CHANNEL_MESSAGES_NUMBER = 4;
    private static final int CHANNEL_FILES_NUMBER = 5;
    private static final int CHANNEL_IS_PRIVATE = 6;

    /** Tabla donde se muestran los canales y su modelo personalizado */
    private final JTable table;
    private final CustomTableModel tableModel;
    /** JButtons de la vista */
    private final JButton addChannelButton;
    private final JButton addUserToChannelButton;
    private final JButton deleteChannelButton;
    private JButton staticsChannelButton;

    /**
     * Constuctor de la vista de los canales
     */
    public ChannelsTabView() {
        setLayout(new BorderLayout());

        /* Nombres a mostrar en las columnas de la tabla */
        String[] columnsName = {"Channel id", "Channel name",
                                "Channel Description",
                                "Users number", "Messages number",
                                "Files number", "Is private?"};

        /* Creamos la tabla y su modelo */
        table = new JTable(new CustomTableModel(columnsName));
        tableModel = (CustomTableModel) table.getModel();
        table.getTableHeader().setReorderingAllowed(false);

        /* JScrollPane donde va la tabla, para hacer scroll si hay muchos datos */
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane, BorderLayout.CENTER);

        /* JPanel donde van los botones */
        JPanel buttonsPanel = new JPanel(new GridLayout());
        add(buttonsPanel, BorderLayout.SOUTH);

        /* Los botones */
        addChannelButton = new JButton("Add channel");
        addUserToChannelButton = new JButton("Add user to chanel");
        deleteChannelButton = new JButton("Delete channel");
        staticsChannelButton = new JButton("Statistics");

        buttonsPanel.add(addChannelButton);
        buttonsPanel.add(addUserToChannelButton);
        buttonsPanel.add(deleteChannelButton);
        buttonsPanel.add(staticsChannelButton);
    }

    /**
     * Añade una nueva fila a la tabla de canales con la informacion de un canal
     * @param channelId id del canal
     * @param channelName nombre del canal
     * @param channelDesc descripcion del canal
     * @param usersNumber numero de usuarios en el canal
     * @param messagesNumber numero de mensajes de text en el canal
     * @param filesNumber numero de archivos(fotos y otros archivos) en el canal
     * @param isPrivate si el canal es privado(chat) o es publico(canal)
     */
    public void addChannelToView(Integer channelId, String channelName, String channelDesc, Integer usersNumber,
                                 Integer messagesNumber, Integer filesNumber, Boolean isPrivate) {
        Object[] row = {channelId, channelName, channelDesc, usersNumber,
                            messagesNumber, filesNumber, isPrivate};
        tableModel.addRow(row);
    }

    /**
     * Elimina un canal recibiendo su id del canal
     * Busca por todas las filas de la tabla y cuando encuentra el id que estamos buscando lo elimina.
     * @param channelId id del canal
     */
    public void deleteChannelToView(Integer channelId) {
        for (int i = 0; i < tableModel.getRowCount(); i++) {
            Integer id = (Integer) tableModel.getValueAt(i, 0);
            if (id.equals(channelId)) {
                tableModel.deleteRow(i);
                break;
            }
        }
    }

    /**
     * Actualiza una fila de la tabla de canales con la informacion nueva del canal,
     * si este no se encuentra se añade una fila nueva al final de la tabla con
     * su informacion
     * @param channelId id del canal
     * @param channelName nombre del canal
     * @param channelDesc descripcion del canal
     * @param usersNumber numero de usuarios en el canal
     * @param messagesNumber numero de mensajes de text en el canal
     * @param filesNumber numero de archivos(fotos y otros archivos) en el canal
     * @param isPrivate si el canal es privado(chat) o es publico(canal)
     */
    public void updateChannel(Integer channelId, String channelName, String channelDesc, Integer usersNumber,
                              Integer messagesNumber, Integer filesNumber, Boolean isPrivate) {
        for (int i = 0; i < tableModel.getRowCount(); ++i) {
            int currentChannelId = (int) tableModel.getValueAt(i, CHANNEL_ID);
            if (currentChannelId == channelId) {
                tableModel.setValueAt(usersNumber, i, CHANNEL_USERS_NUMBER);
                tableModel.setValueAt(messagesNumber, i, CHANNEL_MESSAGES_NUMBER);
                tableModel.setValueAt(filesNumber, i, CHANNEL_FILES_NUMBER);
                return;
            }
        }
        addChannelToView(channelId, channelName, channelDesc, usersNumber,
                messagesNumber, filesNumber, isPrivate);
    }

    /**
     * Muestra el dialog para pedir la informacion del nuevo canal a añadir
     * @return String[], array de 2 Strings, donde en la posicion 0 esta el nombre del canal y
     * en la 1 la descripcion del canal. Devuelve null si han cancelado el dialog
     */
    public String[] showAddChannelDialog() {
        AddChannelDialog dialog = new AddChannelDialog();
        int result = JOptionPane.showConfirmDialog(null, dialog,
                "Enter the new channel information", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            return (new String[] {dialog.getChannelName(), dialog.getChanelDescription()});
        }
        return null;
    }

    /**
     * Muestra el dialog para  pedir la informacion del usuario a añadir en un
     * canal.
     * @return String, email del usuario a añadir en un canal. Deuelve null si
     * han cancelado el dialog
     */
    public String showAddUserToChannelDialog() {
        AddUserToChannelDialog dialog = new AddUserToChannelDialog();
        int result = JOptionPane.showConfirmDialog(null, dialog,
                "Enter the new channel information", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            return (dialog.getUserEmail());
        }
        return null;
    }

    /**
     * Obtiene la id del canal correspondiente a la fila de la tabla seleccionada.
     * En caso de haber mas de uno seleccionado devuelve el del primero seleccionado.
     * @return int, id del canal seleccionado. Devuelve -1 si no hay ninguno seleccionado
     */
    public int getSelectedChannelId() {
        int selectedRow = table.getSelectedRow();
        if(selectedRow < tableModel.getRowCount()) {
            if (selectedRow != -1) {
                return (int) tableModel.getValueAt(selectedRow, CHANNEL_ID);
            }
        }
        return -1;
    }

    /**
     * Obtiene el nombre del canal correspondiente a la fila de la tabla seleccionada.
     * En caso de haber mas de uno seleccionado devuleve el del primero seleccionado
     * @return String, nombre del canal seleccionado. Devuelve null si no hay ninguno
     * seleccionado
     */
    public String getSelectedChannelName() {
        int selectedRow = table.getSelectedRow();
        if (selectedRow != -1) {
            return (String)tableModel.getValueAt(selectedRow, CHANNEL_NAME);
        }
        return null;
    }

    /**
     * Muestra en dialog de error con la informacion que se le proporciona
     * @param title String, titulo del error
     * @param message String, mensaje del error
     */
    public void showError(String title, String message) {
        JOptionPane.showMessageDialog(this, message, title, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Registra los listeners para los botones de la vista
     * @param listener listener de los botones
     */
    public void registerController(ActionListener listener, ListSelectionListener selectionListener) {
        addChannelButton.addActionListener(listener);
        addChannelButton.setActionCommand(ADD_CHANNEL_BUTTON);
        addUserToChannelButton.addActionListener(listener);
        addUserToChannelButton.setActionCommand(ADD_USER_TO_CHANNEL_BUTTON);
        deleteChannelButton.addActionListener(listener);
        deleteChannelButton.setActionCommand(DELETE_CHANNEL_BUTTON);
        staticsChannelButton.addActionListener(listener);
        staticsChannelButton.setActionCommand(STATICS_BUTTON);

        table.getSelectionModel().addListSelectionListener(selectionListener);
    }
}
