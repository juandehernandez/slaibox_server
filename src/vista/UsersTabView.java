package vista;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Panel donde esta la informacion de los usuarios en una tabla y con los
 * botones para hacer las acciones diponibles con ellos
 */
public class UsersTabView extends JPanel {

    /** Constantes para los ActionCommand de los eventos de los botones */
    public static final String BAN_UNBAN_USER_BUTTON = "BAN_UNBAN_USER_BUTTON";

    /** Constantes para identificar las columnas de la tabla de usuarios */
    private static final int USER_EMAIL = 0;
    private static final int USER_USERNAME = 1;
    private static final int USER_STATUS = 2;
    private static final int USER_CHANNELS_NUMBER = 3;
    private static final int USER_LAST_CONNEXION_DATE = 4;
    private static final int USER_REGISTRATION_DATE = 5;

    /** Tabla donde se muestran los canales y su modelo personalizado */
    private final JTable table;
    private CustomTableModel tableModel;

    /** JButtons de la vista */
    private final JButton banUnbanButton;

    /** formatter de la fecha para mostrar fechas por la vista */
    private SimpleDateFormat formatter;

    /**
     * Constructor de la vista de los usuarios
     */
    public UsersTabView() {
        setLayout(new BorderLayout());

        /* Creacion del formatter con el formato que se quiere */
        formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        /* Nombres a mostrar en las columnas de la tabla */
        String[] columnsName = {"User email", "User name", "Status", "Channels number", "Last connexion",
                                "Registration date"} ;

        /* Creamos la tabla y su modelo */
        table = new JTable(new CustomTableModel(columnsName));
        tableModel = (CustomTableModel) table.getModel();

        /* JScrollPane donde va la tabla, para hacer scroll si hay muchos datos */
        JScrollPane scrollPane = new JScrollPane(table);
        add(scrollPane, BorderLayout.CENTER);

        /* JPanel donde van los botones */
        JPanel buttonsPanel = new JPanel(new GridLayout());
        add(buttonsPanel, BorderLayout.SOUTH);

        /* Los botones */
        banUnbanButton = new JButton("Ban/Unban user");
        buttonsPanel.add(banUnbanButton);
    }

    /**
     * Añade una nueva fila a la tabla de usuarios con la informacion de un usuario
     * @param email email del usuario
     * @param username nombre de usuario del usuario
     * @param status estado del usuario
     * @param channelsNumber numero de canales a los que pertenece el usuario
     * @param lastConnectionDate fecha de la ultima conexion del usuario
     * @param registrationDate fecha de registro del usuario
     */
    public void addUserToView(String email, String username, String status, int channelsNumber, Date lastConnectionDate, Date registrationDate) {
        Object[] newRow = {email, username, status, channelsNumber, formatter.format(lastConnectionDate), formatter.format(registrationDate)};
        tableModel.addRow(newRow);
    }

    /**
     * Actualiza una fila de la tabla de usuarios con la informacion nueva del usuario,
     * si este no se encuentra se añade una fila nueva al final de la tabla con su
     * informacion
     * @param email email del usuario
     * @param username nombre de usuario del usuario
     * @param status estado del usuario
     * @param channelsNumber numero de canales a los que pertenece el usuario
     * @param lastConnectionDate fecha de la ultima conexion del usuario
     * @param registrationDate fecha de registro del usuario
     */
    public void updateUser(String email, String username, String status, int channelsNumber, Date lastConnectionDate, Date registrationDate) {
        for (int i = 0; i < tableModel.getRowCount(); ++i) {
            String currentEmail = (String) tableModel.getValueAt(i, USER_EMAIL);
            if (email.equals(currentEmail)) {
                tableModel.setValueAt(status, i, USER_STATUS);
                tableModel.setValueAt(formatter.format(lastConnectionDate), i, USER_LAST_CONNEXION_DATE);
                tableModel.setValueAt(channelsNumber, i, USER_CHANNELS_NUMBER);
                return;
            }
        }
        addUserToView(email, username, status, channelsNumber,
                        lastConnectionDate, registrationDate);
    }

    /**
     * Obtiene el email del usuario correspondiente a la fila de la tabla seleccionada.
     * En caso de haber mas de uno seleccionado devuleve el del primero seleccionado
     * @return String, email del usuario seleccionado. Devuelve null si no hay ninguno
     * seleccionado
     */
    public String getSelectedEmail() {
        int selectedRow = table.getSelectedRow();
        if (selectedRow != -1) {
            return (String) tableModel.getValueAt(selectedRow, USER_EMAIL);
        }
        return null;
    }

    /**
     * Muestra en dialog de error con la informacion que se le proporciona
     * @param title String, titulo del error
     * @param message String, mensaje del error
     */
    public void showError(String title, String message) {
        JOptionPane.showMessageDialog(this, message, title, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Registra los listeners para los botones de la vista
     * @param listener listener de los botones
     */
    public void registerController(ActionListener listener) {
        banUnbanButton.setActionCommand(BAN_UNBAN_USER_BUTTON);
        banUnbanButton.addActionListener(listener);
    }
}
