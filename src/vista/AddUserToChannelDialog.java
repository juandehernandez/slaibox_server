package vista;

import javax.swing.*;
import java.awt.*;

/**
 * JPanel auxiliar para mostar un dialog personalizado para pedir
 * la informacion del usuario a añadir a un canal (el email del usuario)
 */
public class AddUserToChannelDialog  extends JPanel{
    /** JTextField del email del usuario */
    private final JTextField userEmailTF;

    /**
     * Constuctor del dialog de añadir un usario a un canal
     */
    public AddUserToChannelDialog() {
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        userEmailTF = new JTextField();

        JPanel namePanel = new JPanel(new GridLayout());
        namePanel.add(new JLabel("User email:"));
        namePanel.add(userEmailTF);
        add(namePanel);

        setSize(300, 400);
    }

    /**
     * Devuelve el email del usuario escrito
     * @return String, email del usuario
     */
    public String getUserEmail() {
        /* Si el campo esta vacio se devuelve null */
        if (userEmailTF.getText().isEmpty()) {
            return null;
        } else {
            return userEmailTF.getText();
        }
    }
}
