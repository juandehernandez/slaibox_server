package vista;

import org.apache.commons.lang3.ArrayUtils;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Clase que se encarga de generar las gráficas de las estadísticas de un canal
 *
 * @author Miguel Abellán
 * @version 1.5
 */
public class StatisticsTabView extends JPanel{

    private JLabel channelDescription;
    private JLabel lblName;

    private JLabel lblMessagesDescription;
    private JLabel lblMessagesNumber;

    private JLabel lblFilesDescription;
    private JLabel lblFilesNumber;

    private JLabel lblPhotosDescription;
    private JLabel lblPhotosNumber;

    private JLabel[] divisions;

    private ChannelsTabView channelsTabView;

    private Graph messagesGraph;
    private Graph filesGraph;
    private Graph imagesGraph;

    /**
     * Constructor de la clase
     * @param channelsTabView tab
     */
    public StatisticsTabView(ChannelsTabView channelsTabView){

        this.channelsTabView = channelsTabView;

        setLayout(new BorderLayout());

        JPanel top = new JPanel();
        top.setLayout(new BoxLayout(top, BoxLayout.Y_AXIS));
        JPanel[] panels = new JPanel[4];
        for (int i = 0; i < panels.length; i++) {
            panels[i] = new JPanel(new FlowLayout(FlowLayout.CENTER));
            top.add(panels[i]);
        }


        divisions = new JLabel[9];
        JPanel side = new JPanel();
        side.setBackground(Color.WHITE);
        side.setLayout(new GridLayout(divisions.length, 1));
        side.setBorder(BorderFactory.createEmptyBorder(10, 8, 10, 3));

        for (int i = 0; i < divisions.length; i++) {
            divisions[i] = new JLabel();
            JPanel n = new JPanel();
            n.add(divisions[i]);
            side.add(divisions[i]);
        }

        channelDescription = new JLabel("Channel name: ");
        lblMessagesDescription = new JLabel("Number of messages in the last 7 days: ");
        lblFilesDescription = new JLabel("Number of files in the last 7 days: ");
        lblPhotosDescription = new JLabel("Number of pictures in the last 7 days: ");

        lblName = new JLabel("N/A");
        lblMessagesNumber = new JLabel("0");
        lblFilesNumber = new JLabel("0");
        lblPhotosNumber = new JLabel("0");

        panels[0].add(channelDescription);
        panels[0].add(lblName);

        panels[1].add(lblMessagesDescription);
        panels[1].add(lblMessagesNumber);

        panels[2].add(lblFilesDescription);
        panels[2].add(lblFilesNumber);

        panels[3].add(lblPhotosDescription);
        panels[3].add(lblPhotosNumber);

        add(top, BorderLayout.NORTH);

        messagesGraph = new Graph();
        imagesGraph = new Graph();
        filesGraph = new Graph();

        JTabbedPane tabs = new JTabbedPane();
        tabs.addTab("Messages", messagesGraph);
        tabs.addTab("Images", imagesGraph);
        tabs.addTab("Files", filesGraph);
        add(tabs, BorderLayout.CENTER);
    }

    /**
     * Funcion que se encarga de encontrar el nombre del canal que se ha seleccionado.
     * @return devuelve el nombre del canal que se ha seleccionado en la tab de canales.
     */
    public String getChannelName() {
        if(channelsTabView.getSelectedChannelId() != -1) {
            return channelsTabView.getSelectedChannelName();
        } else {
            return "No channel selected";
        }
    }

    /**
     * Setter del nombre del canal
     * @param name nombre del canal
     */
    public void setChannelName(String name) {
        lblName.setText(name);
    }

    /**
     * Setter del numero de mensakes
     * @param nMessages numero de mensajes
     */
    public void setMessagesNumber(int[] nMessages) {
        messagesGraph.setValues(Arrays.asList(ArrayUtils.toObject(nMessages)));
        lblMessagesNumber.setText(String.valueOf(total(nMessages)));
    }

    /**
     * Setter del numero de files
     * @param nFiles numero de files
     */
    public void setFileNumber(int[] nFiles) {
        filesGraph.setValues(Arrays.asList(ArrayUtils.toObject(nFiles)));
        lblFilesNumber.setText(String.valueOf(total(nFiles)));
    }

    /**
     * Setter del numero de imagenes
     * @param nImages numero de imagenes
     */
    public void setImagesNumber(int[] nImages) {
        imagesGraph.setValues(Arrays.asList(ArrayUtils.toObject(nImages)));
        lblPhotosNumber.setText(String.valueOf(total(nImages)));
    }

    /**
     * setter del total
     * @param array array de enteros
     * @return total
     */
    private int total(int[] array) {
        int sum = 0;
        for (int n : array) {
            sum += n;
        }
        return sum;
    }

    /**
     * Clase auxiliar para los graficos
     */
    private class Graph extends JPanel {

        private Stroke thinner;
        private Stroke thin;
        private Stroke bold;
        private Stroke pointStroke;
        private Font calibri;
        private Font calibri30;
        private int max;
        private List<Integer> values;

        /**
         * Constructor del grafico
         */
        public Graph() {
            thinner = new BasicStroke(0.5f);
            thin = new BasicStroke(1.5f);
            bold = new BasicStroke(3f);
            pointStroke = new BasicStroke(5f);
            calibri = new Font("Calibri", Font.PLAIN, 12);
            calibri30 = new Font("Calibri", Font.PLAIN, 30);
            setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
            setBackground(Color.WHITE);
        }

        /**
         * Setter de los valores
         * @param values valores
         */
        public void setValues(List<Integer> values) {
            this.values = values;
            repaint();
        }

        /**
         * Pinta la grafica
         * @param g2 grafico
         */
        @Override
        protected void paintComponent(Graphics g2) {
            super.paintComponent(g2);
            Graphics2D g = (Graphics2D) g2;
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            g.setFont(calibri);
            FontMetrics font = g.getFontMetrics();

            int width = getWidth();
            int height = getHeight();
            int divisionX = Math.round((width - 70) / 8.0f);
            int divisionY = Math.round((height - 40) / 8.0f);

            g.setStroke(bold);
            g.setColor(Color.BLACK);

            //eje X
            g.drawLine(50, height - 20, width - 20, height - 20);

            //eje Y
            g.drawLine(50, 20, 50, height - 20);

            for (int i = 0; i < 8; i++) {

                g.setStroke(thinner);
                g.setColor(Color.LIGHT_GRAY);
                g.drawLine(50 + divisionX * (i + 1), 20, 50 + divisionX * (i + 1), height - 20); //lineas verticales
                g.drawLine(50, 20 + divisionY * i, width - 20, 20 + divisionY * i);

                g.setStroke(bold);
                g.setColor(Color.BLACK);
                g.drawLine(48, 20 + divisionY * i, 52, 20 + divisionY * i); //divisiones eje Y
                g.drawLine(50 + divisionX * (i + 1), height - 22, 50 + divisionX * (i + 1), height - 18); //divisones eje X
            }

            if (values != null) {

                max = Collections.max(values);
                double division = max / 7.0;

                g.setColor(Color.BLACK);
                g.setStroke(thin);
                for (int i = 0; i < 8; i++) {
                    String str = String.valueOf(i);
                    int ancho = font.stringWidth(str);
                    g.drawString(str, 50 + divisionX * (i) - ancho / 2, height - 5); //numeros eje X
                }

                if (max > 0) {
                    g.setFont(calibri);
                    for (int i = 0; i < divisions.length; i++) {
                        String str = String.valueOf(Math.round((max + division) - i * division));
                        int ancho = font.stringWidth(str);
                        g.drawString(str, 25 - ancho / 2, i * divisionY + 20 + 3); //numeros eje Y
                    }

                    for (int i = 0; i < values.size() - 1; i++) {
                        int current = values.get(i);
                        int currentPoint = Math.round((current * (height - 40)) / (max + (int) division));
                        int nextPoint = Math.round((values.get(i + 1) * (height - 40)) / (max + (int) division));

                        g.setStroke(thin);
                        g.setColor(Color.BLUE);
                        g.drawLine(50 + divisionX * (i + 1), height - 20 - currentPoint,
                                50 + divisionX * (i + 2), height - 20 - nextPoint);

                        g.setStroke(pointStroke);
                        g.drawOval(50 + divisionX * (i + 1) - 1, height - 20 - currentPoint - 1, 2, 2);
                        g.setStroke(thinner);
                        g.fillOval(50 + divisionX * (i + 1) - 2, height - 20 - currentPoint - 2, 4, 4);

                        g.setStroke(pointStroke);
                        g.drawOval(50 + divisionX * (i + 2) - 1, height - 20 - nextPoint - 1, 2, 2);
                        g.setStroke(thinner);
                        g.fillOval(50 + divisionX * (i + 2) - 2, height - 20 - nextPoint - 2, 4, 4);

                        /*
                        g.setColor(Color.BLACK);
                        String point = String.format("(%d, %d)", i + 1, current);
                        int strWidth = font.stringWidth(point);
                        g.drawString(point, 10 + divisionX * (i + 1) - strWidth / 2, height - currentPoint);

                        Rectangle back = new Rectangle(10 + divisionX * (i + 1) - strWidth / 2,
                                height - currentPoint, strWidth, g.getFont().getSize());
                        g.draw(back);*/
                    }
                } else {
                    for (int i = 0; i < values.size() - 1; i++) {
                        int currentPoint = 50 + divisionX * (i + 1);
                        int nextPoint = 50 + divisionX * (i + 2);

                        g.setStroke(thin);
                        g.setColor(Color.BLUE);
                        g.drawLine(currentPoint, height - 20, nextPoint, height - 20);

                        g.setStroke(pointStroke);
                        g.drawOval(50 + divisionX * (i + 1) - 1, height - 20 - 1, 2, 2);
                        g.setStroke(thinner);
                        g.fillOval(50 + divisionX * (i + 1) - 2, height - 20 - 2, 4, 4);

                        g.setStroke(pointStroke);
                        g.drawOval(50 + divisionX * (i + 2) - 1, height - 20 - 1, 2, 2);
                        g.setStroke(thinner);
                        g.fillOval(50 + divisionX * (i + 2) - 2, height - 20 - 2, 4, 4);
                    }

                    g.setColor(Color.BLACK);
                    for (int i = 0; i < divisions.length; i++) {
                        String str = "0";
                        int ancho = font.stringWidth(str);
                        g.drawString(str, 25 - ancho / 2, i * divisionY + 20 + 3); //numeros eje Y
                    }
                }
            } else {
                g.setFont(calibri30);
                String str = "Data unavailable";
                int ancho = g.getFontMetrics().stringWidth(str);
                g.drawString(str, width / 2 - ancho / 2, height / 2 - 20 + 15);
                //g.drawString(str, 30, 30);
            }
        }
    }
}