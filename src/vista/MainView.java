package vista;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;

/**
 * Vista principal del servidor, contiene una JTabbedPane con los usuarios,
 * canales y estadisticas
 */
public class MainView extends JFrame{

    /** JTabbedPane donde van los paneles/tabs de usuarios, canales y estadisticas */
    private final JTabbedPane jTabbedPane;

    /** Panel de los canales */
    private final ChannelsTabView channelsTabView;

    /** Panel de los usuarios */
    private final UsersTabView usersTabView;

    /** Panel de las estadisticas */
    private final StatisticsTabView statisticsTabView;

    /**
     * Constructor de la MainView
     */
    public MainView(){
        jTabbedPane = new JTabbedPane();
        add(jTabbedPane, BorderLayout.CENTER);

        usersTabView = new UsersTabView();
        channelsTabView = new ChannelsTabView();
        statisticsTabView = new StatisticsTabView(channelsTabView);

        jTabbedPane.addTab("USERS", usersTabView);
        jTabbedPane.addTab("CHANNELS", channelsTabView);
        jTabbedPane.addTab("STATISTICS", statisticsTabView);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new Dimension(750, 607));
    }

    /**
     * Getter del panel de los canales
     * @return panel de los canales
     */
    public ChannelsTabView getChannelsTabView() {
        return channelsTabView;
    }

    /**
     * Getter del panel de los usuarios
     * @return panel de los usuarios
     */
    public UsersTabView getUsersTabView() {
        return usersTabView;
    }

    /**
     * Getter del panel de las estadisticas
     * @return panel de las estadisticas
     */
    public StatisticsTabView getStatisticsTabView() {
        return statisticsTabView;
    }

    /**
     * Añade el listener a la MainView junto con sus tabs de usuarios, canales
     * y estadisticas
     * @param actionListener listener a añadir
     */
    public void registerController(ActionListener actionListener, ListSelectionListener selectionListener, WindowListener windowListener) {
        channelsTabView.registerController(actionListener, selectionListener);
        usersTabView.registerController(actionListener);

        addWindowListener(windowListener);
    }

    /**
     * Muestra el tab de las estadisticas
     */
    public void showStatisticsTab() {
        jTabbedPane.setSelectedComponent(statisticsTabView);
    }
}