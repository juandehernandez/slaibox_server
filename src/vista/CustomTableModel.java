package vista;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * Tabla propia que contiene filas con columnas de diferentes objetos. Fila compuesta por array de objetos.
 * Uso para la vista del server y para el manejo de cada usuario o canal (modificaciones, selecciones, etc).
 *
 * @author Sergi Valbuena
 * @version 07/05/2017
 */
public class CustomTableModel extends AbstractTableModel {
    private String[] columnsNames = null;
    private List<Object[]> data = null;

    /**
     * Constructor
     * @param columnsNames nombres de las columnas
     */
    public CustomTableModel(String[] columnsNames) {
        super();
        this.columnsNames = columnsNames;
        data = new ArrayList<>();
    }

    /**
     * Devuelve cuantas filas hay
     * @return numero de filas
     */
    @Override
    public int getRowCount() {
        return data.size();
    }

    /**
     * Cuantas columnas hay
     * @return numero de columnas
     */
    @Override
    public int getColumnCount() {
        return columnsNames.length;
    }

    /**
     * Obtiene el valor de una celda
     * @param rowIndex fila
     * @param columnIndex columna
     * @return valor de la celda
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data.get(rowIndex)[columnIndex];
    }

    /**
     * Obtiene el nombre de una columna
     * @param column columna
     * @return nombre de la columna
     */
    @Override
    public String getColumnName(int column) {
        return columnsNames[column];
    }

    /**
     * Añade una fila nueva
     * @param rowData informacion de la nueva columna
     */
    public void addRow(Object[] rowData) {
        data.add(rowData);
        fireTableRowsInserted(0, getRowCount());
    }

    /**
     * Elimina una fila
     * @param position posicion de la fila
     */
    public void deleteRow(int position) {
        data.remove(position);
        fireTableRowsDeleted(0, getRowCount());
    }

    /**
     * Actualiza el valor de una celda
     * @param newValue nuevo valor de la celda
     * @param rowIndex fila
     * @param columnIndex columna
     */
    public void setValueAt(Object newValue, int rowIndex, int columnIndex) {
        data.get(rowIndex)[columnIndex] = newValue;
        fireTableCellUpdated(rowIndex, columnIndex);
    }
}