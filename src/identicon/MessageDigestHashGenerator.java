package identicon;

import java.security.MessageDigest;

/**
 * Clase para geerar un hash
 */
public class MessageDigestHashGenerator implements HashGeneratorInterface {
	private MessageDigest messageDigest;

	/**
	 * Constructor
	 * @param algorithim algorismo a usar
	 */
	public MessageDigestHashGenerator(String algorithim) {
		try {
			messageDigest = MessageDigest.getInstance(algorithim);
		}catch(Exception e) {
			System.err.println("Error setting algorithim: " + algorithim);
		}
	}

	/**
	 * Genera un hash
	 * @param input string input
	 * @return hash
	 */
	public byte[] generate(String input) {
		return messageDigest.digest(input.getBytes());
	}
}
