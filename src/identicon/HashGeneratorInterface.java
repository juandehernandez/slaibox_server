package identicon;

/**
 * Interfaz de generador de hash para el identicon
 */
public interface HashGeneratorInterface {

	/**
	 * Genera un hash a partir de un nombre de usuario
	 * @param userName nombre de usuario
	 * @return hash generado
	 */
	byte[] generate(String userName);
}
